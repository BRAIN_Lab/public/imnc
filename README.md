# Integrated Modular Neural Control (IMNC)

## Introduction

This project provides an integrated modular neural control for versatile locomotion and object transportation of a dung beetle-like robot, ALPHA [1]. As such, the control approach is synthesized based on modular neural mechanisms, integrating CPG-based control, local leg control, descending modulation control, and object manipulation control. While the CPG-based control provides basic locomotion patterns, the local leg control uses sensory feedback and memory mechanisms, enabling ALPHA to perform robust and adaptive locomotion behavior on various terrains (i.e., flat and uneven terrains) while negotiating an obstacle. We explore the descending modulation pathway to modulate the basic patterns and achieve omnidirectional locomotion like forward/backward walking, left/right curve walking, and left/right turning. In addition, the object manipulation control expands the robot’s ability to grasp and transport a variety of objects (i.e., rigid boxes, and hard and soft balls) on flat and uneven terrains. . 

## Framework

The project is organized by three sub-folders including **controllers**, **projects**, and **utils**. 

- **controllers** contains code of the neural control, and ros interface to communicate with the robot driver.
- **project** contains ROS catkin-workspace to build the project.
- **utils** stores the libraries of artificial neural network framework (ann).

## Reference

[1] P. Billeschou, N. N. Bijma, L. B. Larsen, S. N. Gorb, J. C. Larsen, and P. Manoonpong, “Framework for Developing Bio-Inspired Morphologies for Walking Robots,” Appl. Sci., vol. 10, no. 19, p. 6986, 2020, doi: 10.3390/app10196986.

If you have any questions. Feel free to contact me at binggwong.l_s17@vistec.ac.th

## Supplementary Material
For Supplementary Material please see [Supplementary Material](Supplementary_Material.md)




