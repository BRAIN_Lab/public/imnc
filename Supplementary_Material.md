# Supplementary Material
<div align="center">
    <strong>
    Integrated Modular Neural Control for Versatile Locomotion and Object Transportation of a Dung Beetle-Like Robot
    </strong>
</div>

<div align="center">
    Binggwong Leung, Peter Billeschou, and Poramate Manoonpong
</div>

## Supplementary Methods

### *A. PSN network*

The structure of the PSN is shown in Fig. S1. The synaptic connections between the neurons P<sub>1,8</sub> and P<sub>2,6</sub> of the original version [1] have been changed from excitation to inhibition to obtain short swing and long stance leg trajectories in both forward and backward walking for stable locomotion. The input connections from the neuron I<sub>PSN0</sub> and bias of P<sub>4</sub> are shown in Fig. S1. The other weights of the PSNs are the same as those in the previous work [1]. The PSN network can be activated by exciting the input neuron I<sub>PSN0</sub>, thereby switching the phase of two output signals of neurons P<sub>13</sub> and P<sub>14</sub>. In this work, the output signal of the neuron P<sub>13</sub> of each PSN network is projected to the BC motor neuron of each leg. The output signal of the neuron P<sub>14</sub> of each PSN network is projected to the CF and FT motor neurons of each leg.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_PSN.jpg" align="center" width=25%>

Fig. S1. Example of the neural connections of the PSN of the leg L1.
</table>

### *B. The CPG of the Object Manipulation Control*
An SO2 based CPG model with neuromodulation is used for object manipulation control [2]. The model is similar to the CPG-based control (i.e., a recurrent network with two fully connected neurons C<sub>1</sub> and C<sub>2</sub>) but has a different modulatory input (*MI*) value. The CPG can be described by the equations below. In our work, the *MI* value of the additional CPG is equal to 0.2. Thus, the frequency of the additional CPG is higher than that of the CPG-based control where *MI* is 0.05.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_CPGm.png" align="center" width=40%>

</table>

where *O<sub>1</sub>*, *O<sub>2</sub>* are the outputs of the neurons C<sub>1</sub> and C<sub>2</sub>. *W<sub>11</sub>*, *W<sub>22</sub>* are fixed synapses. *W<sub>12<sub>m</sub></sub>*, *W<sub>21<sub>m</sub></sub>* are modulated synapses.

<br />

### *C. Integrated Modular Neural Control Algorithms*
The pseudo code of the CPG-based control, adaptive local leg control, ground searching control with memory, swing reflex control with memory, descending modulation control, and object manipulation control are shown by the table below. For real implementation of the code, please see the git repository of our project (https://gitlab.com/BRAIN_Lab/public/imnc).

Note that although the proposed integrated modular neural control is based on existing generic neural modules such as CPG, PCPG, VRN, PSN, and local leg control [2]. Their integration under the control architecture, that can generate various walking patterns of the dung beetle-like robot with non-uniform leg configurations and can mimic the dung beetles’ pallet transportation behavior, has not been explored before. In this work, we recombine these neural modules to function as versatile locomotion and object manipulation/transportation control. Specifically, the CPG-based control network (Fig. 4 of the main manuscript) is redesigned to achieve multiple locomotion patterns. In addition, the adaptive local leg control for leg trajectory adaptation is here driven by not only sensory feedback but also internal memory (encoding the information of the leg extension from the previous movement cycle). This leads to better adaptation (see Fig. 10 of the main manuscript) compared to the local leg control in the previous work which only relies on sensory feedback. Furthermore, we also introduce the object manipulation control and integrate it into the CPG-based control to enable the robot to manipulate or grasp and transport a variety of objects on different terrains (flat ground, rough terrain, slopes) in the real world. This achievement has not been demonstrated before [1]-[3]. A summary comparison between the previous control [2] and the integrated control with several new features is shown in Table S2.

This integrated control allows the robot to perceive different terrains (terrain/ground depth and height levels) as follows.

To perceive the terrain/ground depth level, the CF-joint torque of each leg is employed to detect the foot condition during a stance phase (Fig. 7 of the main manuscript). In this phase, if the foot is not in contact with the ground, foot contact error in the stance phase ($` e_{st, i}(t) `$, Eq. (4) of the main manuscript) will occur and drive the leg extension until the leg is in contact with the ground through the ground searching control. Additionally, a neural memory mechanism in the control stores the previous foot contact error value and quickly adapts the amplitude of leg extension in the next stance phase. The neural memory output for the adaptive leg extension is different when the leg experiences different ground levels, as shown in Fig. 7(d) of the main manuscript; the deeper the ground, the larger the output value. Therefore, different terrain depth levels can be recognized through the neural memory output (Fig. 7(d) of the main manuscript).

To perceive the terrain/ground height level, the BC-joint torque of each leg is employed to detect the leg condition during a swing phase (Fig. 8 of the main manuscript). In this phase, if the leg hits the terrain or an obstacle at a higher height than normal, terrain/obstacle hitting error ($` e_{sw, i}(t)`$, Eq. (10) of the main manuscript) will occur, driving the leg to swing higher until it can avoid hitting through the swing reflex control. Additionally, a synaptic plasticity mechanism in the control can online adapt the amplitude of the swing reflex movement with respect to the terrain/obstacle height. The synaptic strength for the adaptive leg elevation is different when the leg experiences different terrain/obstacle heights, as shown in Fig. 8(d) of the main manuscript; the higher the terrain/obstacle, the larger the synaptic value. Therefore, different terrain height levels can be recognized through the synaptic strength (Fig. 8(d) of the main manuscript).

Taken together, each leg can independently perceive different terrains from the motor joint torque feedback and the neural memory and plasticity mechanisms. This approach allows the robot to traverse complex terrains without complex sensors.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_algo1.png" align="center" width=50%>
</table>
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_algo2.png" align="center" width=50%>
</table>
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_algo3.png" align="center" width=50%>
</table>
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_algo4.png" align="center" width=50%>
</table>
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_algo5.png" align="center" width=50%>
</table>
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_algo6.png" align="center" width=50%>
</table>
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_ts1.png" align="center" width=50%>
</table>
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_ts1.2.png" align="center" width=50%>
</table>
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_ts2.png" align="center" width=50%>
</table>

<br />

### *D. Energy Consumption of the Robot System*
In this work, we analyze the energy/power consumption of the system by measuring the cost of transport (COT). The COT is calculated as follows:

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_COT.png" align="center" width=60%>

</table>

where *P<sub>com</sub>* is the power consumption of computing resource (raspberry pi 3b+) calculated from *I* (the average electric current of the computing resource (i.e., 600 mA)) and *V* (the voltage of the computing resource (i.e., 5 V)). *P<sub>motors</sub>* is the power consumption of 21 dynamixel motors with 12 V supply. The average motor current is measured from motor current sensors. *g* is the gravitational acceleration (~9.81 m/s2). *s* is the robot distance travelled and *t* is the time used for traveling over a given distance. *m* is the transferred gross weight. For only locomotion, *m* is the weight of the robot (i.e., 4.3 kg). For object transportation, *m* is the total weight of the robot and object.

</br>

### *E. Coordination Mechanisms of Integrated Modular Neural Control*
The proposed neural control consists of four main neural network modules including 1) CPG-based control, 2) descending modulation control, 3) adaptive local leg control, and 4) object manipulation control. The coordination mechanisms between the modules are described below. 

The CPG-based control is the main module for locomotion generation. It is based on a hierarchical layer architecture consisting of a CPG network (top layer) that generates rhythmic signals, which are transmitted to a pattern formation network (PCPGs, PSNs, VRNs, middle layer) and then the motor neurons (bottom layer). 

The descending modulation control (orange box in Fig. S2) is used to control or activate different robot walking behaviors (i.e., forward walking, backward walking, left/right curve walking, and left/right spot turning). Currently, an operator command is used to manually activate a certain behavior. The output signal of the descending modulation control can either excite/activate the PSN networks (orange arrows in Fig. S2) or shunt/reduce neural activities projected from the VRNs to the PSNs or the PSNs to the motor neurons (dashed orange lines in Fig. S2).

The object manipulation control can be activated when the robot needs to grasp or manipulate an object. Two mechanisms involved in this control are activated manually by an operator command. The first mechanism is to change the motor neuron biases of the hind leg joints ($`BC_{2,5}`$, $`CF_{2,5}`$, $`FT_{2,5}`$). This is to set the legs in a proper grasping position. The second mechanism generates rhythmic signals to the motor neurons of the leg joints to periodically move the legs during object transportation. The first mechanism is designed to transport a basketball and a cardboard box while the second one is for transporting a soft ball.

The adaptive local leg control (ALLC) is distributed to control each leg. The ALLC of each leg receives the efference copy signals (the VRN output signals) from the pattern formation network in the CPG-based control. The ground searching control and swing reflex control of the ALLC are activated by the forward model, which predicts the swing and stance of the leg. The ALLC receives the sensory feedback signals (motor position and torque) which are used in the forward model, ground searching control, and swing reflex control. The ALLC output is transmitted to each leg's motor neurons, modulating motor neuron activities for adaptive leg trajectories on uneven terrain.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_Coordination.jpg" align="center" width=50%>

Fig. S2. Coordination mechanisms between neural network control modules.

</table>
<br />

## Supplementary Experimental Results

### *A. Object Transportation*
The ball pushing experiment is shown in Fig. 12 of the main manuscript. The additional details of the joint angle for this experiment are shown in Supplementary Fig. S3 and S4. Fig. S3 shows the joint position command signals while transporting the soft ball on uneven terrain. Fig. S4 shows the joint position command signals while transporting the basketball, cardboard box, and soft ball on flat terrain. The weight of the basketball, box, and soft ball are 0.6, 0.12, and 0.15 kg, respectively. The object transportation speed for the basketball, box, and soft ball on flat terrain are approximately 8.5, 6, and 5 cm/s, respectively. The object transportation speed for the soft ball on uneven terrain is approximately 4 cm/s. We encourage the reader to see Supplementary Video S5 for transporting a very heavy object (i.e., a box with water bottles having a total weight of 4.7 kg).

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_jointsignal2.jpg" align="center" width=50%>

Fig. S3. Joint position command signals during soft ball transportation on uneven terrain.

</table>
<br />

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_jointsignal.jpg" align="center" width=50%>

Fig. S4. Joint position command signals for different object transportation behavior. The hard ball (basketball) has a diameter of 25 cm (70% of leg length). The box measures 17x25x18 cm (width = 50% of leg length). The soft ball (fitness ball) has a diameter of 20 cm (60% of leg length).

</table>
<br />

### *B. Omnidirectional Locomotion*
Various motor patterns of different locomotion programs are shown in Fig. S5(b). At the beginning of the experiment, the robot stood on the floor with all joints in a fixed position due to the biases of the motor neurons. The robot then started walking forward. When started walking backward, the motor pattern of all motors was changed due to the modulation of all PSNs. After the robot finished walking backward, it started to perform left curve walking. The motor pattern changed back into forward walking, except for the amplitude in the left legs which was reduced by the shunting mechanism. The robot then performed forward walking and the amplitude of the left legs increased in accordance with the forward walking motor pattern. The robot then performed right curve walking with reduced amplitude in the right legs. Then, the robot started to turn left in accordance with the changed motor pattern in the left side legs because the PSNs of those legs were modulated. Finally, the robot turned right by modulating the PSNs of the right side legs. Specifically, during the turning behavior, all signals to the FT-motors became inhibited, causing all FT-motor joints to remain in a certain position. The gait diagram is shown in Fig. S5(c). A tripod was observed in all omnidirectional locomotion behaviors. However, some artifacts on the gait diagram show double stepping of the leg. Since this experiment only used the CPG-based control, the leg trajectories did not adapt to the ground.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_omnidirectional.jpg" align="center" width=40%>

Fig. S5.  Omnidirectional locomotion of the dung beetle-like robot ALPHA. (a) Snapshots of omnidirectional behavior. (b) Joint position command (degree) of each joint with respect to each locomotor pattern including forward and backward walking, left and right curve walking, left and right turning (see Supplementary Video S2). (c) Gait diagram of omnidirectional locomotion. The omnidirectional locomotion on uneven terrain is also shown in Supplementary Video S2.
</table>
<br />

### *C. Uneven Terrain Adaptation*
The ground searching control was tested for uneven terrain adaptation using two scenarios involving the wooden block terrain. In the first scenario, the terrain had a hole in the middle (Fig. S6(a)), while the second scenario consisted of downward step terrain (Fig. S7(a)). In both scenarios, we compared the performance of the CPG-based control with and without the ground searching control by using walking speed and stability analysis (Fig. S8). The initial moment unit (PhidgetSpatial precision 3/3/3 High-Resolution IMU, ID:1044_1) sensor was also attached to the robot’s head to measure its orientation for stability calculation. In each experimental run, the robot walked for 200 time-step (7 seconds). 

In the first scenario, the robot stood on the terrain with a hole in the middle, as shown in Fig. S6(a). The robot walked forward to the left direction (Fig. S6(b)). Fig. S6(b) shows the side view of the walking snapshots of the CPG-based control with and without the ground searching control. The red marker indicates the robot’s body position while walking. The red marker for the control with the ground searching control shows that the robot could walk further in comparison to the CPG-based control only. In the third snapshot of each condition (Fig. S6(b)), the robot using the ground searching control already walked past the hole. While in the other control, the middle legs still stepped in the hole (see Supplementary Video S3) and became stuck. The walking system using the ground searching control shows CF-joint adaptation in the stance phase (Fig. S6(c)). Due to the leg extending toward the ground with the ground searching control, the gait pattern showed the middle legs touching the ground earlier when stepping in the hole (Fig. S6(d)). The robot’s roll angle which represents the tilting angle of the robot to the left and right sides of the body is shown in Fig. S6(e). The locomotion control using ground searching control showed that after tilting to the right and left when stepping in the hole, the robot slowly converged to the initial roll angle. Whereas when walking without the ground searching control, the robot tilted more between left and right. The walking speed and stability are shown in Fig. S8. The angular velocity and acceleration of the robot are used to calculate the walking stability. The robot walks faster with the ground searching control in the hole terrain than when using only the CPG-based control. This is due to the leg extension in the hole providing more foot contact to propulse the body forward. For walking stability, the CPG-based control with the ground searching control enables the robot to walk better with greater stability (Fig. S8).  

In the second scenario, the robot stood at the top of the downward step terrain as shown in (Fig. S7(a)). It then walked forward down the steps. The front view snapshots of the robot are shown in Fig. S7(b). The locomotion control in both conditions controlled the robot to walk forward down the steps. However, as indicated by the final snapshots in Fig. S7(b), the robot without the ground searching control rotated in the yaw axis due to instability (See Supplementary Video S3). The rotation around the yaw axis of the robot could be clearly seen at 23 s (Fig. S7(d)). The leg extension of the ground searching control (Fig. S7(c)) kept the robot stable because the leg could adapt the height to retain contact with the ground and support the robot. We also show the stability of multiple experimental runs in Fig. S8. The robot walked with greater stability and slightly faster when using the ground searching control rather than only the CPG-based control in the downward step terrain.

The stability of the robot locomotion is calculated using the stability index from previous work on a walking hexapod [5]. The acceleration (*Acc*) and angular velocity (*Ang*) of each axis from the initial moment unit (IMU) are used to calculate stability. In the previous work, the minimum acceleration $`Acc_{min}`$ of the experimental run is used for calculation [5]. However, in this work, we use the maximum acceleration $`Acc_{max}`$ instead to capture the high acceleration of robot locomotion. The $`Acc_{max}`$ from each axis is calculated by (7). $`Acc_0`$ is the initial value when the robot initially stands. The peak to peak angular velocity $`Ang_{pp}`$ is calculated from the maximum angular velocity $`max(Ang)`$ and minimum angular velocity $`min(Ang)`$ by (8). The maximum peak to peak angular velocity $`PP_{max}`$ is then calculated by (9). Finally, the stability index is calculated using the average exponential decays of $`Acc_{max}`$ and $`PP_{max}`$ by (10). A higher stability index value means that the robot walks with better stability.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_stable.png" align="center" width=50%>
</table>

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_GS.jpg" align="center" width=25%>

Fig. S6. Locomotion across the hole terrain using CPG-based control with and without the ground searching control. (a) Initial foot placement of the robot and terrain dimension. (b) Side view walking snapshots of the robot. The red marker indicates the body position of the robot. (c) CF-joint angle commands. (d) Gait patterns of the middle legs (L2, R2). (e) The robot roll angle from five runs in each condition. GSC means the ground searching control with memory.
</table>
<br />
 
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_GS_stepdown.jpg" align="center" width=25%>

Fig. S7. Locomotion across the downward step terrain using the CPG-based control with and without the ground searching control. (a) Initial foot placement of the robot and terrain dimension. (b) Front view walking snapshots of the robot. (c) CF-joint angle command. (d) Robot yaw angle from five runs in each condition. GSC means the ground searching control with memory.
</table>
<br />

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_GS_performance.jpg" align="center" width=25%>

Fig. S8. Walking speed and stability analysis of locomotion on the hole and downward step terrain of the CPG-based control with and without the ground searching control with memory (GSC). Stability is calculated from (10).
</table>
<br />

### *D. Obstacle Negotiation*
Here, we tested the swing reflex system by allowing the robot to walk on rough terrain toward the upward step obstacle measuring 5, 10, and 15 cm in height (Fig. S9(a)). The starting distance between the obstacle and the front legs of the robot is 10 cm. We allowed the robot to run for 400 time-step (15 seconds) and recorded the movement if it successfully climbed the obstacle within the allocated time. We activated the ground searching control and compared the controller with and without the swing reflex control. 

The swing reflex error signals processed from the BC-joint torque are shown in Fig. S9(b). The swing reflex error signal of leg R1 increased when it hit the step obstacle. The error signal then drove the joint to rotate in the opposite direction (Fig. S9(c)) to avoid the obstacle. Small peaks in the swing reflex error occurred when the leg hit the small rock on the rough terrain while high peak errors occurred when the leg hit the step obstacle. The high peak swing reflex error signal occurred in the both sides of the leg (e.g., L1, L2, L3, R1, R2, R3), first in the front leg and then in the posterior legs. The signal mostly occurred in the middle of the swing phase. However, some signals occurred during the late swing phase in leg R3. This happened when the leg touched the ground early before the stance phase, causing robot instability because leg R3 might not be able to support the body initially in the next stance phase (see Supplementary Video S4). We also performed five trials of experiments for each obstacle height to observe the rate of success when climbing on the obstacle. Fig. S9(d) shows the success rate of the system with and without the swing reflex control. The robot with the swing reflex control achieved a 100% success rate on the 5 cm high step obstacle (15% of leg length) and 60% on the 10 cm step (30% of leg length). However, the robot could not climb the 15 cm high step obstacle (45% of leg length). The robot without the swing reflex could not climb all the 5, 10, and 15 cm high step obstacles. This indicates that the robot leg with the swing reflex was able to react and avoid when hitting the obstacle.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_SW.jpg" align="center" width=25%>

Fig. S9. Locomotion on uneven terrain with obstacles using CPG-based control with the swing reflex control. (a) Terrain description. (b) Swing reflex error signals. (c) BC-joint angles. (d) Success rate for obstacle negotiation with and without the swing reflex control. The step obstacle heights of 5 cm, 10 cm, and 15 cm are equal to 15%, 30%, and 45% of leg length, respectively.
</table>
<br />

### *E. Locomotion Capability*
We tested the locomotion capability of our robot in both forward and backward walking on different slope angles (Fig. S10). The robot could walk forward and backward on a downward slope up to -20 degrees. It could walk forward up to 20 degrees on an upward slope while walking backward it could only manage up to 5 degrees. When the slope was steeper than the ranges, the robot began to slip.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_LocLim.jpg" align="center" width=50%>

Fig. S10. The capability of forward and backward locomotion on different slope angles (see also Supplementary Video S6).
</table>
<br />

### *F. Object Transportation Capability*
We tested the object transportation capability (moving an object with backward walking) of our robot on different slope angles, object types (cuboid, sphere), object weights, and object sizes. (Fig. S11). Overall, the robot could transport a sphere object (soft ball) up to 2 kg on a 0 degree slope and 1.5 kg on a 5 degree slope. The robot did not need to transport the ball down the slope since the ball rolled down the slope by itself. The robot could transport a cuboid object (paper box) up to 4 kg on a 0 degree slope and 1 kg on a 5 degree slope. We also tested the box transportation on a -10 degree (downward) slope. The robot could transport up to 4 kg with a low cost of transport (COT) on the downward slope. The box started to slide by itself down when the slope was steeper than -10 degrees. The maximum weight capacity that the robot could transport was reduced on an upward slope because the robot had to also bear the object weight. In addition, the data distribution of the ball with 1.0 and 1.5 kg are not statistically different (Mann Whitney test, p-value > 0.05). The maximum object size that the robot could transport was approximately 25 cm wide. This was limited by the configuration of the robot's hind legs, whereas the length and height of the object could be greater than 25 cm as long as the robot legs were capable of grasping the object.
 
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_ObjTranLim.jpg" align="center" width=50%>

Fig. S11. The capability of object transportation on different slope angles, object types, and object weights (see also Supplementary Video S6). The data distribution in the soft ball with 1.0 and 1.5 kg weights is not statistically different (Mann Whitney test, p-value > 0.05).
</table>
<br />

### *G. Friction Test*
We performed the friction force measurements in both locomotion and object transportation behaviors using a digital push-pull force gauge (model: HF-50).

For locomotion, we attached a rope with the force gauge and let the robot walk to measure the leg-ground friction. Fig. S12 shows the force ($`F_m`$) that the legs can generate on flat and uneven terrains. For forward walking (Fig. S12(a)), the average friction is approximately 20 N on the flat terrain and approximately 25 N on the uneven terrain. The leg-ground friction on the uneven terrain is higher than on the flat terrain. For backward walking (Fig. S12(b)), the average friction is approximately 5 N on the flat and uneven terrains. The experiments clearly show that forward walking can generate a higher friction force, especially on the uneven terrain, compared to the other cases. This could be due to the mechanical interlocking between the (middle/hind) legs and the unevenness of the terrain surface [6], [7] which may occur often and easily during this uneven terrain forward walking. In fact, there are several factors that lead to different friction forces in forward and backward walking on different terrains. The factors include different orientations between the front and middle/hind legs, different forward and backward leg trajectories, and the weight distribution of the robot, which may not perfectly align at the center of the robot.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_frictestLoco.jpg" align="center" width=50%>

Fig. S12. Leg-ground friction force experiments of locomotion. (a) Forward locomotion. (b) Backward locomotion. *V* represents the movement direction of the robot. $`F_m`$ is the force measured from the force gauge. There are a hundred samples (n=100) in each condition.
</table>
<br />

For object transportation, there are three types of experiments to investigate the object-ground friction, leg-ground friction, and leg-object friction. The conceptual diagrams of all experiments are shown in Fig. S13. The object transportation can be modeled as a diagram in Fig. S13(a). The friction between the object and the ground (object-ground friction, $`F_{og}`$) prevents the object from moving while the front and middle legs provide the leg-ground friction ($`F_{lg}`$) to push the object. There is also the leg-object friction (Fig. S13(d), 13(e), 13(f)) which the hind legs used to hold an object. All these friction forces contribute to a total force ($`F_{tot}`$) for pushing an object (Fig. S13(a)). Therefore, we will first investigate the object-ground friction, then the leg-ground friction, and finally the leg-object friction.

<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_frictestDiagram.jpg" align="center" width=50%>

Fig. S13. Diagrams showing experiments to investigate the leg-ground friction, object-ground friction, and leg-object friction of the object transportation behavior. (a) Total force test for pushing an object. (b) Object-ground friction test. (c) Estimating the robot’s weight distributed on an object. (d) Leg-object downward friction test. (e) Leg-object upward friction test. (f) Leg-object horizontal friction test. $`F_m`$ represents the measurement force of all experiments. $`N_o`$ represents the ground reaction force of the ground from the weight of the object. $`N_{tot}`$ represents the ground reaction force of the ground from the weight of the object and robot. *V* represents the movement direction of the robot or object.
</table>
<br />

We used the equation ($` F_{og} = \mu_s N_{tot} = \mu_s (mg + W_{robot}) `$) to calculate the object-ground friction ($`F_{og}`$) as shown in Fig. S13(c). $` \mu_s `$ is the coefficient of friction which can be obtained from the experiment in Fig. S13(b). $`N_{tot}`$ represents the ground reaction force derived from the object weight (*mg*) and the robot’s weight distributed on the object ($`W_{robot}`$). This can be obtained from the experiment in Fig. S13(c). First, we need to find the coefficient of friction ($`\mu_s`$) between objects (soft ball, hard ball, and box) and the ground (Fig. S13(b)). We added an additional 1.8 kg on an object to increase the object-ground friction into the range that can be measured by the force gauge. We pulled the object in the horizontal direction until the object slipped on the ground. Then we measured the maximum pulling force which can estimate the object-ground friction force ($`F_{og}=F_m`$). According to this, we calculated the coefficient of friction between the object and ground from the equation ($`\mu_s=F_{og}/mg`$). The coefficient of friction between each object on flat and uneven terrain is shown in Fig. S14(a). We can see that the soft ball has the highest coefficient of friction followed by the hard ball and box. The soft ball has a higher friction due to the deformation leading to a larger contact area, compared to the hard ball with the same shape. This effect is known as contact area-mediated friction (adhesion-mediated friction) [6], [7]. Next, we investigated the robot’s weight distributed on the object ($`W_{robot}`$) as shown in Fig. S13(c). In this experiment, we held the force gauge stationarily above the robot and let the robot perform object transportation behavior (walking backward with front and middle legs). This approach can estimate the dynamic weight which is distributed onto the object while performing object transportation. The distributed weight is shown in Fig. S14(b). The median value is approximately 14 N, and the maximum value is approximately 22 N. Then, we used all the aforementioned parameters to calculate the object-ground friction from the equation ($`F_{og}=\mu_s(mg+W_{robot})`$). We used the average value of the coefficient of friction and distributed weight for the calculation. The object-ground friction is shown in Fig. S14(c). The result suggests that the object-ground friction on the uneven terrain is higher than the one on the flat terrain. This could be due to the mechanical interlocking between the objects and the unevenness of the terrain surface.

Next, we investigated the leg-ground friction force ($`F_{lg}`$) as shown in the Fig. S13(a). In the experiment, we measured the pulling force and let the robot perform object transportation behavior on the empty box. The force measured from the experiment represents a total force driving the system ($`F_{tot}=F_m`$). The total force on flat and uneven terrains is shown in Fig. S14(d). From the result, the total force on the flat and uneven terrain is similar. However, there are some high friction outliers on uneven terrain which might be caused by the mechanical interlocking between legs and the ground. The total force measured from the force gauge was used to estimate the leg-ground friction force for the object transportation. We estimated the leg-ground friction force ($`F_{lg}`$) from the diagram in Fig. S13(a) as follows: $`F_{lg}=F_m+f_{og}`$. The estimated leg-ground friction force is shown in Fig. S14(e). The result shows that the leg-ground friction force is higher than the object-ground friction force in the box condition. Therefore, the robot can successfully transport the box on flat and uneven terrains. In contrast, the leg-ground friction force is lower than the object-ground friction force in the soft ball condition. This result confirms the requirement for the robot to push the soft ball using leg oscillation to reduce the object-ground friction force.

Lastly, to understand the leg-object friction force, we analyzed the maximum friction forces between the hind legs and different types of objects during grasping. To do so, we performed pulling tests in upward (Fig. S13(d)), downward (Fig. S13(e)), and horizontal (Fig. S13(f)) directions to find the corresponding friction forces. The leg-object downward friction test was performed to determine the maximum downward friction force that can withstand the ground reaction force ($`N_{tot}`$) contributed by the object weight (*mg*) and distributed weight ($`W_{robot}`$) of the robot (Fig. S13(d)). The leg-object upward friction test was performed to determine the maximum upward friction force that can prevent the object from falling off when the hind legs oscillated or attempted to lift the object above the ground (Fig. S13(e)). The leg-object horizontal friction test was performed to determine the maximum friction force capable of keeping the object from slipping or rotating caused by the object-ground friction (Fig. S13(f)). Each friction was measured by applying the pulling force in a particular direction until the object slipped. We also measured the contact area between the legs and objects by identifying contact regions and measuring the area inside the regions using a measurement tape. In the experiment, we let the robot grasp a soft (deformable) ball, the straight sides of a rigid paper box, a hard ball, or the corners of a rigid paper box (Fig. S15(a)).
  
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_frictest1.jpg" align="center" width=50%>

Fig. S14. Friction force experiments of object transportation behavior. (a) Coefficient of friction between the objects and the ground. (b) Distributed weight measured from the test of the robot’s weight distributed on an object. (c) Estimated object-ground friction. (d) Total force measured from the total force test. (e) Estimated leg-ground friction. There are 100 samples (n=100) in each condition of experiment (a), (b), (d), (e) and three samples (n=3) in experiment (c).
</table>
<br />
   
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_frictest2.jpg" align="center" width=50%>

Fig. S15. Result of leg-object friction on grasping different objects/conditions. (a) Drawing diagrams and pictures showing grasping of a soft ball, a paper box (straight sides), a hard ball, and a paper box (corners). (b) The contact area of grasping different objects/conditions. (c) Pulling force measurement to determine downward friction force. (d) Pulling force measurement to determine upward friction force. (e) Pulling force measurement to determine horizontal friction force. (f) Downward friction force. (g) Upward friction force. (h) Horizontal friction force. There are three samples (n=3) in each condition.
</table>
<br />

Fig. S15(b) shows that the soft ball has the greatest contact area with the legs, followed by the box (straight sides), hard ball, and box (corners). Figs. S15(f), S15(g), and S15(h) show the friction forces calculated from the pulling force measurement ($`F_m`$, Figs. S15(c), S15(d), and S15(e)). The downward friction force ($`f_d`$) was calculated from $`f_d=F_m-mg`$. The upward friction force ($`f_u`$) was calculated from $`f_u=F_m+mg`$. The horizontal friction force ($`f_h`$) was calculated from $`f_u=F_m`$. The results show that the friction forces positively correlate with the contact area of the legs and object in all conditions. This is known as contact area-mediated friction (adhesion-mediated friction) [6], [7]. Therefore, it is possible to increase the friction force for stable object manipulation by installing deformable or soft materials at the legs, resulting in contact area enhancement. In fact, friction force involves not only contact area-mediated friction but also mechanical interlocking (as observed in the box corner test (Fig. S15(d)). The first one depends on the formation of strong contact between the legs and a (deformable) object. The second one rather depends on the interlocking between the legs and a (non-flat) object (e.g., box corners). Thus, additional information on the contact force between the hind legs and an object will be important to estimate leg-object friction precisely. Note that, in the upward friction test of the hard ball, the ball fell whenever there was no support for the object. Thus, from the equation ($`f_u=F_m+mg`$), we can only estimate that the friction force was less than the weight (~<6 N) of the hard ball (Fig. S15(g)).

During object transportation, the object was subjected to the ground reaction force. In order to keep it stable or prevent it from slipping, the hind legs of the robot had to produce enough grasping force (i.e., downward friction force) to compensate for the ground reaction force. The downward friction force ($`f_d`$) is shown in Fig. S15(f), and the maximum distributed weight ($`W_{robot}`$) is approximately 20 N (Fig. S14(b)). The result shows that when grasping the soft ball and box, the legs provided enough friction force (>21 N) which can prevent the object from slipping off. In contrast, it did not generate enough force to stably grasp the hard ball or corners of the paper box. This is due to the low contact area between the legs and object, resulting in low adhesion-mediated friction (as described above).



### *H. Large Ball Rolling*
  
<table align="center"><tr><td align="center"  width="9999">
<img src="readme_info/supple_ball_rolling.jpg" align="center" width=30%>

Fig. S16. The large ball rolling posture of the ALPHA robot and its biological counterpart.
</table>
<br />

## Supplementary Videos

### **Supplementary Video S1: Object transportation inspired by dung beetle** ([Video S1](https://www.manoonpong.com/IEEE/VideoS1.mp4))
Object transportation behavior of the dung beetle-like robot (ALPHA) on uneven terrain inspired by its biological counterpart. The ALPHA robot could successfully perform long distance object transportation on uneven terrain.

### **Supplementary Video S2: Omnidirectional locomotion** ([Video S2](https://www.manoonpong.com/IEEE/VideoS2.mp4))
The ALPHA robot demonstrated a continuous sequence of six different locomotion behaviors on flat terrain using a tripod gait. The robot performed locomotion behavior in the following order: 1) forward walking, 2) backward walking, 3) left curve walking, 4) right curve walking, 5) left turning, and 6) right turning.

### **Supplementary Video S3: Ground searching control** ([Video S3](https://www.manoonpong.com/IEEE/VideoS3.mp4))

Locomotion across the uneven terrain using CPG-based control with and without the ground searching control of the ALPHA robot. Two testing scenarios were performed on the wooden block terrain, namely a terrain with a hole and a downward steps terrain. In each experimental run, the robot walked for 200 time-step (7 seconds). The ground searching control system demonstrated successful walking across the uneven terrains without the robot becoming stuck in the hole and tilting when stepping down.

### **Supplementary Video S4: Swing reflex control** ([Video S4](https://www.manoonpong.com/IEEE/VideoS4.mp4))

Locomotion on uneven terrain with an obstacle using the CPG-based control with and without the swing reflex. The ALPHA robot was tested by allowing it to walk on rough terrain toward obstacles measuring 5 and 10 cm in height. The starting distance between the obstacle and front legs of the robot was 10 cm. We allowed the robot to move 900 time-step (15 seconds). It also performed different adaptation speeds of the synaptic plasticity adaptation mechanism by using different learning rate ($`\alpha`$) values. The walking system using the swing reflex control demonstrated successful walking across the uneven terrain and obstacle by performing a swing reflex movement when the leg hit the obstacle. 

### **Supplementary Video S5: Object transportation** ([Video S5](https://www.manoonpong.com/IEEE/VideoS5.mp4))
A combination of locomotion and manipulation control was used on the ALPHA robot to transport different objects (i.e., basketball, box, soft fitness ball) on flat and uneven terrain. The robot used different strategies to transport the various objects, for example, it grasped the object with its hind legs and walked backward with the remaining four legs to transport the basketball and a box with water bottles (4.7 kg) on flat terrain. Whereas the robot transported the soft fitness ball by grasping it while also oscillating its hind legs and using the remaining legs to walk backward on both flat and uneven terrain. We also compared the performance between the CPG-based control and the CPG-based control with the adaptive local leg control.

### **Supplementary Video S6: Locomotion & object transportation on slope** ([Video S6](https://www.manoonpong.com/IEEE/VideoS6.mp4))
The locomotion and object transportation capabilities of our robot. Examples of the robot performance during forward and backward walking as well as transporting different objects (soft ball, box) on upward and downward slopes. 

<br />



## Reference

[1] C. T. L. Sørensen and P. Manoonpong, “Modular Neural Control for Object Transportation of a Bio-inspired Hexapod Robot,” in From Animals to Animats 14, 2016, pp. 67–78, doi: 10.1007/978-3-319-43488-9_7.

[2] Manoonpong P, Parlitz U, Wörgötter F. Neural control and adaptive neural forward models for insect-like, energy-efficient, and adaptable locomotion of walking machines. Front Neural Circuits. 2013 Feb 13;7:12. Available from https://www.frontiersin.org/article/10.3389/fncir.2013.00012

[3] B. Leung, M. Thor, and P. Manoonpong, “Modular Neural Control for Bio-Inspired Walking and Ball Rolling of a Dung Beetle-Like Robot,” in The 2018 Conference on Artificial Life, 2018, pp. 335–342.

[4] P. Manoonpong, F. Pasemann, and H. Roth, “Modular Reactive Neurocontrol for Biologically Inspired Walking Machines,” Int. J. Rob. Res., vol. 26, no. 3, pp. 301–331, Mar. 2007.

[5] Ngamkajornwiwat P, Homchanthanakul J, Teerakittikul P, Manoonpong P. Bio-inspired Adaptive Locomotion Control System for Online Adaptation of a Walking Robot on Complex Terrains. IEEE Access. 2020;8:1–1.

[6] S. Gorb, Attachment Devices of Insect Cuticle. Springer Dordrecht, 2001.

[7] H. T. Tramsen, S. N. Gorb, H. Zhang, P. Manoonpong, Z. Dai, and L. Heepe, “Inversion of friction anisotropy in a bio-inspired asymmetrically structured surface,” J. R. Soc. Interface, vol. 15, no. 138, p. 20170629, Jan. 2018.

