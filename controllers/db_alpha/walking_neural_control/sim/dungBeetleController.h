//
// Created by Carlos on 27/02/2019.
// From Neutron Modular Controller by Mathias Thor.
//

#ifndef DUNG_BEETLE_CONTROLLER_DUNGBEETLECONTROLLER_H
#define DUNG_BEETLE_CONTROLLER_DUNGBEETLECONTROLLER_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <tuple>
#include <vector>
#include <math.h>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "simRosClass.h"
#include "../dbModularController.h"
#include "../dbMotorDefinition.h"
#include "delayline.h"
#include "../dualIntegralLearner.h"
#include "../postProcessing.h"
#include "signalPatterns.h"
#include "../AMC.h"
//#include "joystick.h"


#define PI 3.14159265359

class dualIntegralLearner;
class modularController;
class simRosClass;
class postProcessing;
class signalPatterns;
class AMC;

class dungBeetleController 
{
    public:
        int countgg = 0;
        // Constructor 
        dungBeetleController(int argc,char* argv[]);
        
        // Methods
        bool runController();

        // Variable
        float degtoRad = PI/180.0;
        float fac = 1.0;
        float c1, c1h, c2, c2h;

    private:
        
        // Private Methods
        void actuateRobot();

        // Numerical
        double rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter); 

        // Logging data
        void logData(double simtime, double CPGphi, double error, double maxVel, double maxForce, double positionX, double bodyVel, 
                    double angularVelocity, double jointTorque, double controllerOut, double systemFeedback, double hlNeuronAF, double hlNeutronDL);
        void logJointData(double simTime, vector<float> torques, vector<float> joinPoss, vector<float> LPFtorques, vector<float> jointVels);
        void logPPData(double simTime, double ampSensor, double ampController, double LPFSensor, double LPFController, double error, double abs_error);
        void logFeedbackData(double simTime, float pos_feedback, float pos_sent, float vel_feedback, float vel_sent, float des_vel, float torque, double dt);
        void logTCData(double simTime, float st, float at, float apt, float sp, float dp, float sv, float dv, double dt);
        


        // Variables
        ofstream myfile;
        ofstream MiF;
        ofstream MiddleLegsFile;
        ofstream FrontLegsFile;
        ofstream BackLegsFile;
        ofstream mnnData;
        ofstream torqueData;
        ofstream LPFtorqueData;
        ofstream jointPosData;
        ofstream jointVelData;
        ofstream PPData;
        ofstream fbData;
        ofstream TCData;
        ofstream AMCData;

        vector<float> motor_values;
        //vector<float> torques;
        std::vector<float> data;
        vector<Delayline> tauLeftDelayLine;
        vector<Delayline> tauDelayLine;

        // Controller configurations
        bool isSimulation = true;
        float MAX_TORQUE = 4.8;
        int tau=250;       // Tripod 18/45
        int tauLeft=250;   // Wave
        int tmpTau=0;
        bool positive = true;
        bool paintDirection = false;
        bool controlType;
        double verticalArmHight = 0;
        bool legged_or_arm = true;
        double waiter = 200; // 5 SECONDS
        double triggerCount = 0;
        double CPGamp = 0.0;
        float hfOffset = 0.0;
        float MotorOutput4Rescaled=0;
        float MotorOutput4Neg = 0;
        float MotorOutput3Neg = 0;
        float MotorOutput0Neg = 0;
        double joySpeed = 0;
        double joyTurn = 0;
        bool useJoy = false;

        // Motor limits:
        float minFL = 0.0;
        float minML = 0.0;
        float minBL = 0.0;

        // AMC data.
        vector<float> applied_torque;
        //vector<float> desired_velocities = {0,0,0,0,0,0,0,0,0,0,0,0,0.2,0,0,0,0,0};
        vector<float> desired_velocities = {0,0}; 
        //vector<float> desired_positions = {0,0,0,0,0,0,0,0,0,0,0,0,0.3,1.0,1.4,1.4,1.0,1.4};
        vector<float> desired_positions = {-1.0,0};
        //vector<float> desired_positions = {0,0,0,0,0,0,0.25,0,0,0,0,0,1.0,0,0,0,0,0};

        std::vector<float> targetBC = { 30.0,   10.0,  -30.0};		// joint bias manual setting
        std::vector<float> targetCF = {-75.0,  -60.0,  -30.0};
        std::vector<float> targetFT = {-0.0,  -30.0,    9.0};

        vector<float> dung_beetle_pose = {0.2619, -1.2222, -0.5238,
        								  0.2619, -1.2222, -0.5238,
                                          0.2794, -1.0476, -0.5238,
                                          0.2794, -1.0476, -0.5238,
                                          0.3492, -1.5714, -0.3492,
                                          0.3492, -1.5714, -0.3492,
                                          -0.2885, 0.349206349, 0.0};

        vector<float> current_velocities;
        vector<float> current_positions;
        float previous_positionCF = 0; // Should be made a vector
        float previous_positionFT = 0;
        float prev_valueCF = 0;
        float prev_valueFT = 0;
        float velCF0, des_velCF0;
        double current_simTime;
        double previous_simTime = 0;

        // AMC optimization
        float optB = 0.0001;
        float prev_feedback_torque = 0;
        float feedback_torque = 0;
        float actual_torque_error = 0;
        float prev_torque_error = 0;

        // CPG (this is intended just for data collection and understanding)
        float _o1_cpg = 0;
        float _o2_cpg = 0;
        float _o1_pcpg = 0;
        float _o2_pcpg = 0;

        // PMN Outputs
        float _o1_pmn = 0;
        float _o2_pmn = 0;
        float _o3_pmn = 0;
        float _o4_pmn = 0;
        float _o5_pmn = 0;

        
        int savings = 0;
        double FT_pos;

        /* 0. Vanilla CPG SO2
        * 1. Dual Learner
        * 2. Adaptive frequency
        * 3. DL + AF -> NN (Hard Limiter and SigmoidII) */
        int CPGmethod = 0;
        double errorMargin = 0.000;
        double weightBiasHL = 0.004;
        double weightBiasHLTwo = 0.0015;


        // Object declarations
        dualIntegralLearner * learner;
        modularController * CPGAdaptive;
        simRosClass * simRos;
        postProcessing * sensorPostprocessor;
        postProcessing * controllerPostprocessor;
        postProcessing * sensorAmpPostprocessor;
        postProcessing * controllerAmpPostprocessor;
        postProcessing * CPGAmpPostprocessor;
        postProcessing * LPFPertu;
        signalPatterns * db_pattern;
        AMC * complianceController;
        //Joystick * joystick;
        
};


#endif //DUNG_BEETLE_CONTROLLER_DUNGBEETLECONTROLLER_H
