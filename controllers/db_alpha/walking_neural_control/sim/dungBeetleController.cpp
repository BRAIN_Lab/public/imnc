//
// Created by Carlos on 27/02/2019.
// From Neutron Controller by Mathias Thor.
//

#include "dungBeetleController.h"


//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

// Object creator
dungBeetleController::dungBeetleController(int argc,char* argv[]) 
{
    // Create object pointer instances
    simRos = new simRosClass(argc, argv); // SIMROS
    db_pattern = new signalPatterns(); // SIGNALPATTERNS
    CPGAdaptive = new modularController(1); // MODULARCONTROLLER
    learner = new dualIntegralLearner(CPGmethod != 3, 20);
    sensorPostprocessor = new postProcessing(); // POSTPROCESSING
    sensorPostprocessor->setBeta(0.25);
    controllerPostprocessor = new postProcessing(); // POSTPROCESSING
    controllerPostprocessor->setBeta(0.25);
    sensorAmpPostprocessor = new postProcessing(); // POSTPROCESSING
    sensorAmpPostprocessor->setBeta(0.04);
    controllerAmpPostprocessor = new postProcessing(); // POSTPROCESSING
    controllerAmpPostprocessor->setBeta(0.04);
    CPGAmpPostprocessor = new postProcessing(); // POSTPROCESSING
    CPGAmpPostprocessor->setBeta(0.5);
    complianceController = new AMC(0.05, 35.0, 5.0); //change to 5
    //complianceController = new AMC(0.05, 10, optB);

    /*if (legged_or_arm)
        learner = new dualIntegralLearner(CPGmethod != 3, 20);
    else
        learner = new dualIntegralLearner(true, 20);*/

    // Vector carrying motor commands
    motor_values.resize(22);
    applied_torque.resize(2);
    //torques.resize(22);
    //motor_values.at(21) = optB;

    for (int i = 0; i < 12; ++i) 
    {
        if(i == 2 || i == 3) tmpTau=tau+1; else tmpTau=tau*2+1;
        Delayline tmp(tmpTau);
        tauDelayLine.push_back(tmp);
    }

    for (int k = 0; k < 4; ++k) 
    {
        Delayline tmp(tauLeft+1);
        tauLeftDelayLine.push_back(tmp);
    }

    complianceController->getParams();

    // Get initial values
    previous_simTime = simRos->simulationTime;
    previous_positionCF = simRos->jointPositions[6]; // CF0 (Torque Control)
    previous_positionFT = simRos->jointPositions[12]; // FT0 (Position Control)

    //--data--data--data--data--data--data--data--data--data--data--data--data--data--data--data--data--data--data--data--data--

    // Data Storing

    myfile.open ("../V-REP_DATA/log_file.dat");
    myfile << "time\tcpgPhi\terror\tmaxVel\tmaxTorque\tpositionX\tbodyVel\tangularVelocity\tjointTorque\tcontrollerOut\tsystemFeedback\thlNeuronAF\thlNeutronDL" << "\n";

    myfile.close();
    myfile.open ("../V-REP_DATA/log_file.dat", std::ios_base::app);
    
    // Do not forget to change paths. 
    // General instalation space of gorobots: /home/user/workspace/gorobots
    // Maybe "/home/user/workspace/dung_beetle_experiments/*.csv"
    MiddleLegsFile.open("/home/binggwong/experiments/genfiles/middle_leg_motors.csv");
    FrontLegsFile.open("/home/binggwong/experiments/genfiles/front_leg_motors.csv");
    BackLegsFile.open("/home/binggwong/experiments/genfiles/PMN_values.csv");
    mnnData.open("/home/binggwong/experiments/genfiles/MNN_data.csv");
    torqueData.open("/home/binggwong/experiments/genfiles/joint_torques.csv");
    LPFtorqueData.open("/home/binggwong/experiments/genfiles/joint_torques_LPF.csv");
    jointPosData.open("/home/binggwong/experiments/genfiles/joint_positions_sensed.csv");
    PPData.open("/home/binggwong/experiments/genfiles/post_processing_data.csv");
    jointVelData.open("/home/binggwong/experiments/genfiles/joint_velocities_sensed.csv");
    fbData.open("/home/binggwong/experiments/genfiles/feedback_position_control.csv");
    TCData.open("/home/binggwong/experiments/genfiles/FT0_torque_control.csv");
    AMCData.open("/home/binggwong/experiments/genfiles/AMC_data.csv");

    CPGAdaptive->setInputNeuronInput(0, 1);
    CPGAdaptive->setInputNeuronInput(1, 1);
    CPGAdaptive->setInputNeuronInput(2, 1);
    CPGAdaptive->setInputNeuronInput(3, 1);
}


// Function that runs the controller and keeps it active
bool dungBeetleController::runController() 
{
    double amplitudeSensor, amplitudeController;
    double LPFSensorAmp, LPFControllerAmp;
    double error;
    double true_error;

    if(ros::ok()) 
    {
        //std::cout << "test " << 1 << std::endl;
        simRos->synchronousSimulation(static_cast<unsigned char>(false));
        //std::cout << "test " << 2 << std::endl;
        simRos->rosSpinOnce();
        //std::cout << "test " << 3 << std::endl;
        if(triggerCount >= 10) // todo does not work, has to manully send: rostopic pub --once /triggerNextStep std_msgs/Bool false
            simRos->triggerSim();
        else 
        {
            simRos->triggerSimNoWait();
            triggerCount++;
        }

        // Post Processing module (these might be the signals I need to get the real angle value)
        CPGamp = CPGAmpPostprocessor->calculateLPFAmplitude(CPGAdaptive->getCpgOutput(1));
        amplitudeSensor = sensorPostprocessor->calculateLPFAmplitude(simRos->jointPositions[0]);
        amplitudeController = controllerPostprocessor->calculateLPFAmplitude(motor_values.at(BC4)); // TODO ISN'T IT FIXED?
        LPFSensorAmp = sensorAmpPostprocessor->calculateLPFSignal(amplitudeSensor);
        LPFControllerAmp = controllerAmpPostprocessor->calculateLPFSignal(amplitudeController);
        error = (LPFControllerAmp-errorMargin) - LPFSensorAmp;
        true_error = LPFControllerAmp - LPFSensorAmp;

        if(waiter >= 0) 
        {
            error = 0;
            waiter--;
        }
        else
        {
            waiter = -10;
        }

        // 2580 min value

        // New Phi for robot
        double MI = 0;
        MI = simRos->testParameters[2];


        // Setup and calculate Neurons Output
        double bias = 1;

        // Sigmoid
        // 0.05 error = -350 8
        // 0.005 error = -2500
        // 0.004 = -4000 6
        //double leftNeuronOutputSIGTwo = 1 / (1 + exp(-(-4000*error + 6*bias)));
        double leftNeuronOutputSIGTwo = (error > (0+weightBiasHLTwo*bias) ? 0 : 1);

        // Hard limiter
        //weightBiasHL = 0.01;
        int leftNeuronOutputHL = (error > (0+weightBiasHL*bias) ? 0 : 1);
        int rightNeuronOutputHL = (error > (0+weightBiasHL*bias) ? 1 : 0);

        CPGAdaptive->setMI(MI);

        // New AMC Parameters
        //actual_torque_error = applied_torque.at(0) - simRos->jointTorques[6];
        //optB = complianceController->optimization(optB, complianceController->fabs(actual_torque_error));
        //complianceController->setBeta(optB);
        float newA = simRos->testParameters[0];
        float newB = simRos->testParameters[1];
        float newBeta = simRos->testParameters[3];
        complianceController->setA(newA);
        complianceController->setB(newB);
        complianceController->setBeta(newBeta);

        /*
            vector<double> testParameters:
                            0. AMC a
                            1. AMC b
                            2. Phi
                            3. AMC beta
                            4. Position X
                            5. SMA Velocity
                            6. Body Velocity
                            7. Joint Torque CF0
                            8. Joint Torque FT0
                            9. Linear Velocity (X)
                            10. Linear Velocity (Y)
                            11. Step Counter
        
        */

        // Log Data
        logData(simRos->simulationTime, CPGAdaptive->getPhi(), true_error,
                simRos->testParameters[3], simRos->testParameters[0],
                simRos->testParameters[4], simRos->testParameters[6],
                simRos->testParameters[7], simRos->testParameters[8],
                motor_values.at(BC4), simRos->jointPositions[0], leftNeuronOutputSIGTwo, rightNeuronOutputHL);
        
        // Record robot data sample
        /*if(savings<2000)
        {
            // Log Post Processing data
            logPPData(simRos->simulationTime, amplitudeSensor, amplitudeController, LPFSensorAmp, LPFControllerAmp, error, true_error);
            // Log positions and sensor data
            logJointData(simRos->simulationTime, simRos->jointTorques, simRos->jointPositions, simRos->jointLPFTorques, simRos->jointVelocities);
        }*/

        // Take step with CPGs
//        cout << "CPGstep" << endl;
        CPGAdaptive->step();
//        countgg++;
//        cout << countgg << endl;

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

        // V-REP's ONLINE PLOT

        data.clear();
        data.push_back(CPGAdaptive->getFinalNeuronOutput(0));
        data.push_back(motor_values.at(BC0));
        
        // RED
//        data.push_back(_o1_pcpg); // POSITION CONTROL: Interpolated velocity at CF0.
        //data.push_back(desired_positions.at(0)); // TORQUE CONTROL: Desired Position at CF0.
        //data.push_back(simRos->jointTorques[12]); // Torque fb on CF0

        // GREEN
        //data.push_back(applied_torque.at(0)); // AMC torque at CF0
//        data.push_back(motor_values.at(CF0)); // POSITION CONTROL: Position value at CF0.
        //data.push_back(desired_positions.at(1)); // TORQUE CONTROL: Desired position at FT0.
        //data.push_back(desired_velocities.at(0));
        

        // YELLOW
        //data.push_back(applied_torque.at(1)); // AMC torque at FT0
//        data.push_back(_o1_cpg); // POSITION CONTROL: Position Feedback at CF0.
        //data.push_back(motor_values.at(FT0)); // TORQUE CONTROL: Torque applied at FT0.
        
        // BLUE
        //data.push_back(0);
//        data.push_back(0); // POSITION CONTROL: Velocity Feedback at CF0.
        //data.push_back(simRos->jointPositions[6]); // TORQUE CONTROL:  Position Feedback at CF0.
        //data.push_back(simRos->jointVelocities[6]);

        // PINK
//        data.push_back(0); // POSITION CONTROL: Desired velocity at CF0.
        //data.push_back(desired_velocities.at(1)); // TORQUE CONTROL: Desired velocity at FT0.
        //data.push_back(simRos->jointTorques[12]); // Torque fb on FT0
        //data.push_back(desired_positions.at(1));
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

        // Get controller type
        controlType = simRos->getControllerType();

        // Actuate robot
        actuateRobot();

        // ROS Spin
        simRos->rosSpinOnce();

        // Termination of the simulation
        if ((simRos->simState != 1 && waiter < 180) || simRos->terminateSimulation) 
        {
            // Print Goodbye Messages
            cout << "Position Controller = " << controlType << endl;
            cout << "Shutting down the controller." << endl;
            
            // Close data files
            myfile.close();
	        MiF.close();
            MiddleLegsFile.close();
            FrontLegsFile.close();
            BackLegsFile.close();
            mnnData.close();
            torqueData.close();
            LPFtorqueData.close();
            jointPosData.close();
            PPData.close();
            jointVelData.close();
            fbData.close();
            TCData.close();
            AMCData.close();

            // Shutdown ROS node
            ros::shutdown();

            // Delete objects
            delete simRos;
            delete CPGAdaptive;
            delete learner;
            delete sensorPostprocessor;
            delete controllerPostprocessor;
            delete sensorAmpPostprocessor;
            delete controllerAmpPostprocessor;
            delete CPGAmpPostprocessor;
            return !simRos->terminateSimulation;
        }
        else 
        {
            return !simRos->terminateSimulation;
        }

    } else
    {
        cout << "Shutting down the controller." << endl;
        return false;
    }
}


//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-


// Data log function
void dungBeetleController::logData( double simtime, double CPGphi, double error, double maxVel, double maxForce,
                                 double positionX, double bodyVel, double angularVelocity, double jointTorque,
                                 double controllerOut, double systemFeedback, double hlNeuronAF, double hlNeutronDL)
{
    myfile << simtime <<"\t"<< CPGphi <<"\t"<< error <<"\t"<< maxVel <<"\t"<< maxForce <<"\t"<< positionX <<"\t" << bodyVel << "\t" << angularVelocity << "\t" << jointTorque << "\t" << controllerOut << "\t" << systemFeedback << "\t" << hlNeuronAF << "\t" << hlNeutronDL << "\n";
}


// Sensor data log function
void dungBeetleController::logJointData(double simTime, vector<float> torques, vector<float> joinPoss, vector<float> LPFtorques, vector<float> jointVels)
{
    // Torque
    torqueData << simTime;
    for(size_t i=0; i<torques.size(); i++)
    {
        torqueData << "," << torques[i];
    }
    torqueData << endl;
    LPFtorqueData << simTime;
    for(size_t k=0; k<LPFtorques.size(); k++)
    {
        LPFtorqueData << "," << LPFtorques[k];
    }
    LPFtorqueData << endl;

    // Joint Positions sensed in the simulation
    jointPosData << simTime;
    for(size_t j=0; j<joinPoss.size(); j++)
    {
        jointPosData << "," << joinPoss[j];
    }
    jointPosData << endl;

    // Joint velocities sensed in the simulation
    jointVelData << simTime;
    for(size_t j=0; j<jointVels.size(); j++)
    {
        jointVelData << "," << joinPoss[j];
    }
    jointVelData << endl;

    // Positions vector: Motor commands sent by the controller
    MiF << simTime;
    for(int v=0; v<18; v++)
    {
	    MiF << "," << motor_values[v];
    }
    MiF << endl;
}


// Log Post Processing Data
void dungBeetleController::logPPData(double simTime, double ampSensor, double ampController, double LPFSensor, double LPFController, double error, double abs_error)
{
    PPData << simTime << "," << CPGamp << "," << ampSensor << "," << ampController << "," << LPFSensor << "," << LPFController << "," << error << "," << abs_error << endl; 
}

void dungBeetleController::logFeedbackData(double simTime, float pos_feedback, float pos_sent, float vel_feedback, float vel_sent, float des_vel, float torque, double dt)
{
    fbData << simTime << "," << pos_feedback << "," << pos_sent << "," << vel_feedback << "," << vel_sent << "," << des_vel << "," << torque << "," << dt << endl;
}

// Log Torque Control data of one joint
void dungBeetleController::logTCData(double simTime, float st, float at, float apt, float sp, float dp, float sv, float dv, double dt)
{
    TCData << simTime << "," << st << "," << at << "," << apt << "," << sp << "," << dp << "," << sv << "," << dv << "," << dt << endl; 
}


//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-


// Y-Axis Re-scaling function (Amplitude)
double dungBeetleController::rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter)
{
    return (((newMax-newMin)*(parameter-oldMin))/(oldMax-oldMin))+newMin;
}


//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-


// Function that actuates motors each cycle
void dungBeetleController::actuateRobot() 
{
    // CENTRAL PATTERN GENERATION

    // Get CPG values
    _o1_cpg = CPGAdaptive->getCpgOutput(0);
    _o2_cpg = CPGAdaptive->getCpgOutput(1);

    // Post-CPG Values
    _o1_pcpg = CPGAdaptive->getpcpgOutput(0);
    _o2_pcpg = CPGAdaptive->getpcpgOutput(1);

    // Pre-Motor Network Values
    _o1_pmn = CPGAdaptive->getFinalNeuronOutput(0); // Connected from neuron PSN_h10 with w = 5
    _o2_pmn = CPGAdaptive->getFinalNeuronOutput(1); // Connected from neuron PSN_h11 with w = 5
    _o3_pmn = CPGAdaptive->getFinalNeuronOutput(2); // Connected from neuron PSN_h11 with w = -5
    _o4_pmn = CPGAdaptive->getFinalNeuronOutput(3); // Connected from neuron VRN_left with w = -2.5
    _o5_pmn = CPGAdaptive->getFinalNeuronOutput(4); // Connected from neuron VRN_right with w = -2.5

    tau=4;

    tauLeft=simRos->testParameters[1];

    // MOVEMENT DIRECTION

    double turnFactorLEFT = 1;
    double turnFactorRIGHT = 1;
    double hfOffsetSignLEFT = 1;
    double hfOffsetSignRIGHT = 1;

    // Get and rescaled MNN outputs
    float maxNetworkOut = 1.0;
    float MotorOutput0, MotorOutput1, MotorOutput2, MotorOutput3, MotorOutput4, MotorOutput5, MotorOutput6, MotorOutput7, MotorOutput8, MotorOutput9;

    // FT Joints remain constant for now
    float FT_pos = 1.0;

    // MotorOutput3 and MotorOutput4 go for BC Joints (Proximal)
    MotorOutput4 = rescale(1, -1, 0.25, -0.25, CPGAdaptive->getpcpgOutput(1));
    MotorOutput3 = rescale(1, -1, 0.1, -0.1, CPGAdaptive->getpcpgOutput(1));
    MotorOutput0 = rescale(1, -1, 0.2, 0.0, CPGAdaptive->getpcpgOutput(1));
    MotorOutput9 = rescale(1, -1, 0.2, 0.0, -CPGAdaptive->getpcpgOutput(1));
    MotorOutput4Neg = -MotorOutput4;
    MotorOutput3Neg = -MotorOutput3;
    MotorOutput0Neg = -MotorOutput0;

    // MotorOutput2 and MotorOutput 1 go to CF Joints (Coxa)
    MotorOutput1 = rescale(1, -1, 0.5, 0.0, CPGAdaptive->getpcpgOutput(0));
    MotorOutput2 = rescale(1, -1, 0.5, 0.0, -CPGAdaptive->getpcpgOutput(0));
    MotorOutput5 = rescale(1, -1, 0.7, 0.0, CPGAdaptive->getpcpgOutput(0));
    MotorOutput6 = rescale(1, -1, 0.7, 0.0, -CPGAdaptive->getpcpgOutput(0));
    MotorOutput7 = rescale(1, -1, 0.3, 0.0, CPGAdaptive->getpcpgOutput(0));
    MotorOutput8 = rescale(1, -1, 0.3, 0.0, -CPGAdaptive->getpcpgOutput(0));

    float test_value = rescale(0.2, -0.2, 0.23, 0.2, CPGAdaptive->getCpgOutput(0));
    //float test_value = rescale(0.2, -0.2, 0.005, -0.005, CPGAdaptive->getCpgOutput(0));


//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

    // SEND MOTOR COMMANDS

    if(controlType == true)
    {

 //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

        // JOINT POSITION CONTROL

        // 1. Update time
        current_simTime = simRos->simulationTime;
        double dt = current_simTime - previous_simTime;

        // 2. Interpolate velocity (current)
        float current_pos = simRos->jointPositions[6];
        float dx = current_pos - previous_positionCF;
        velCF0 = (float) dx/dt;

        // 3. Interpolate velocity (desired)
        float des_dx = (-MotorOutput1 - prev_valueCF);
        des_velCF0 = (float) des_dx/dt;

        // 4. Send motor positions
        int forward = -1;
//        motor_values.at(BC0) = -MotorOutput4;
//        motor_values.at(BC1) = -MotorOutput0Neg;
//        motor_values.at(BC2) = -MotorOutput3;
//        motor_values.at(BC3) = MotorOutput4Neg;
//        motor_values.at(BC4) = -MotorOutput9;
//        motor_values.at(BC5) = MotorOutput3Neg;
//
//        motor_values.at(CF0) = -MotorOutput1;
//        motor_values.at(CF1) = -MotorOutput8;
//        motor_values.at(CF2) = -MotorOutput5;
//        motor_values.at(CF5) = -MotorOutput6;
//        motor_values.at(CF4) = -MotorOutput7;
//        motor_values.at(CF3) = -MotorOutput2;
//
//        motor_values.at(FT0) = 1.4;
//        motor_values.at(FT1) = FT_pos;
//        motor_values.at(FT2) = 1.4;
//        motor_values.at(FT5) = 1.4;
//        motor_values.at(FT4) = FT_pos;
//        motor_values.at(FT3) = 1.4;
//
//        // Raw CPG values
//        motor_values.at(18) = _o1_cpg;
//        motor_values.at(19) = _o2_cpg;
//
//        motor_values.at(20) = 0.0;
//        motor_values.at(21) = 1.0;

        ////////////////////////////////////

    	c1  = CPGAdaptive->getFinalNeuronOutput(0);
    	c1h = CPGAdaptive->getFinalNeuronOutput(1);

    	c2  = CPGAdaptive->getFinalNeuronOutput(2);
    	c2h = CPGAdaptive->getFinalNeuronOutput(3);
//    	cout << CPGAdaptive->getFinalNeuronOutput(0) << endl;

    	fac = 1.2;
    	float faccf = 1;

    	//// position = oscillated signal(rad) + joint bias(rad)
    	motor_values.at(BC0) =   fac * c1h * 0.5 	   + ( targetBC[0]  * degtoRad);//( MC1.getFinalNeuronOutput(0) + biasBC0)*degtoRad*rangeBC[0];
    	motor_values.at(BC1) =   fac * c2  * 0.4 	   + ( targetBC[1]  * degtoRad);//( MC1.getFinalNeuronOutput(1) + biasBC1)*degtoRad*rangeBC[1];
    	motor_values.at(BC2) =   fac * c2h * 0.4 	   + ( targetBC[2]  * degtoRad);//( MC1.getFinalNeuronOutput(2) + biasBC2)*degtoRad*rangeBC[2];
    	motor_values.at(BC3) =   fac * c1  * 0.5 	   + ( targetBC[0]  * degtoRad);//( MC1.getFinalNeuronOutput(3) + biasBC3)*degtoRad*rangeBC[0];
    	motor_values.at(BC4) =   fac * c2h * 0.4 	   + ( targetBC[1]  * degtoRad);//( MC1.getFinalNeuronOutput(4) + biasBC4)*degtoRad*rangeBC[1];
    	motor_values.at(BC5) =   fac * c2  * 0.4 	   + ( targetBC[2]  * degtoRad);//( MC1.getFinalNeuronOutput(5) + biasBC5)*degtoRad*rangeBC[2];

    	// Coxa-Femur Joint Position
    	motor_values.at(CF0) =   fac * c2h * 0.7  * faccf     + ( targetCF[0]  * degtoRad);//( MC1.getFinalNeuronOutput(6)  + biasCF0)*degtoRad*rangeCF[0];
    	motor_values.at(CF1) =  -fac * c1  * 0.5   * faccf     + ( targetCF[1]  * degtoRad);//( MC1.getFinalNeuronOutput(7)  + biasCF1)*degtoRad*rangeCF[1];
    	motor_values.at(CF2) =  -fac * c1h * 0.55  * faccf     + ( targetCF[2]  * degtoRad);//( MC1.getFinalNeuronOutput(8)  + biasCF2)*degtoRad*rangeCF[2];
    	motor_values.at(CF3) =   fac * c2  * 0.7  * faccf 	   + ( targetCF[0]  * degtoRad);//( MC1.getFinalNeuronOutput(9)  + biasCF3)*degtoRad*rangeCF[0];
    	motor_values.at(CF4) =  -fac * c1h * 0.5   * faccf     + ( targetCF[1]  * degtoRad);//( MC1.getFinalNeuronOutput(10) + biasCF4)*degtoRad*rangeCF[1];
    	motor_values.at(CF5) =  -fac * c1  * 0.55  * faccf     + ( targetCF[2]  * degtoRad);;//( MC1.getFinalNeuronOutput(11) + biasCF5)*degtoRad*rangeCF[2];

    	// Femur-Tibia Joint Position
    	motor_values.at(FT0) = -fac * c2h * 0.8 *-1.0 + ( targetFT[0]  * degtoRad);//( MC1.getFinalNeuronOutput(12) + biasFT0)*degtoRad*rangeFT[0];
    	motor_values.at(FT1) = -fac * c1  * 0.3        + ( targetFT[1]  * degtoRad);//( MC1.getFinalNeuronOutput(13) + biasFT1)*degtoRad*rangeFT[1];
    	motor_values.at(FT2) = -fac * c1h * 0.3        + ( targetFT[2]  * degtoRad);//( MC1.getFinalNeuronOutput(14) + biasFT2)*degtoRad*rangeFT[2];
    	motor_values.at(FT3) = -fac * c2  * 0.8 *-1.0 + ( targetFT[0]  * degtoRad);//( MC1.getFinalNeuronOutput(15) + biasFT3)*degtoRad*rangeFT[0];
    	motor_values.at(FT4) = -fac * c1h * 0.3        + ( targetFT[1]  * degtoRad);//( MC1.getFinalNeuronOutput(16) + biasFT4)*degtoRad*rangeFT[1];
    	motor_values.at(FT5) = -fac * c1  * 0.3        + ( targetFT[2]  * degtoRad);//( MC1.getFinalNeuronOutput(17) + biasFT5)*degtoRad*rangeFT[2];
//
//    	motor_values.at(BC0) =   0;
//    	motor_values.at(BC1) =   0;
//    	motor_values.at(BC2) =   0;
//    	motor_values.at(BC3) =   0;
//    	motor_values.at(BC4) =   0;
//    	motor_values.at(BC5) =   0;
//
//    	// Coxa-Femur Joint Position
//    	motor_values.at(CF0) =   -90;
//    	motor_values.at(CF1) =  -90;
//    	motor_values.at(CF2) =  -90;
//    	motor_values.at(CF3) =  -90;
//    	motor_values.at(CF4) =  -90;
//    	motor_values.at(CF5) =  -90;
//
//    	// Femur-Tibia Joint Position
//    	motor_values.at(FT0) = 0;
//    	motor_values.at(FT1) = 0;
//    	motor_values.at(FT2) = 0;
//    	motor_values.at(FT3) = 0;
//    	motor_values.at(FT4) = 0;
//    	motor_values.at(FT5) = 0;
//
//
//    	motor_values.at(LONGITUDINAL) = dung_beetle_pose.at(LONGITUDINAL);
    	motor_values.at(TRANSVERSAL) = dung_beetle_pose.at(TRANSVERSAL);
//    	motor_values.at(HEAD) = dung_beetle_pose.at(HEAD);

         // 5. Save data
        if (savings < 2000)
        {  
            // Motor Commands
            
            logFeedbackData(simRos->simulationTime, current_pos, motor_values.at(CF0), simRos->jointVelocities[6], velCF0, des_velCF0, simRos->jointTorques[6], dt);
            FrontLegsFile << simRos->simulationTime << "," << motor_values.at(BC0) << "," << motor_values.at(CF0) << "," << motor_values.at(FT0) << "," << motor_values.at(BC3) << "," << motor_values.at(CF3) << "," << motor_values.at(FT3) << endl;
            MiddleLegsFile << simRos->simulationTime << "," << motor_values.at(BC1) << "," << motor_values.at(CF1) << "," << motor_values.at(FT1) << "," << motor_values.at(BC4) << "," << motor_values.at(CF4) << "," << motor_values.at(FT4) << endl;
            BackLegsFile << simRos->simulationTime << "," << _o1_pmn << "," << _o2_pmn << "," << _o3_pmn << "," << _o4_pmn << "," << _o5_pmn << endl;
            mnnData << simRos->simulationTime << "," << _o1_cpg << "," <<  _o2_cpg << "," << _o1_pcpg << "," << _o2_pcpg << endl;

        }

        // 6. Update Control Values
        previous_simTime = current_simTime;
        previous_positionCF = current_pos;
        prev_valueCF = motor_values.at(CF0);

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

    }
    else
    {

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

        // JOINT TORQUE CONTROL

        // AMC Controller
        
        // 1. Update time
        current_simTime = simRos->simulationTime;
        double dt = current_simTime - previous_simTime;

        // 2. Set desired positions (from CPG)
        /*desired_positions.at(BC0) = -MotorOutput4;
        desired_positions.at(BC1) = -MotorOutput0Neg;
        desired_positions.at(BC2) = -MotorOutput3;
        desired_positions.at(BC3) = MotorOutput4Neg;
        desired_positions.at(BC4) = -MotorOutput9;
        desired_positions.at(BC5) = MotorOutput3Neg;

        desired_positions.at(CF0) = -MotorOutput1;
        desired_positions.at(CF1) = -MotorOutput8;
        desired_positions.at(CF2) = -MotorOutput5;
        desired_positions.at(CF5) = -MotorOutput6;
        desired_positions.at(CF4) = -MotorOutput7;
        desired_positions.at(CF3) = -MotorOutput2;

        desired_positions.at(FT0) = 1.4;
        desired_positions.at(FT1) = FT_pos
        desired_positions.at(FT2) = 1.4;
        desired_positions.at(FT5) = 1.4;
        desired_positions.at(FT4) = FT_pos
        desired_positions.at(FT3) = 1.4;*/
        
        desired_positions.clear();
        desired_positions.resize(2);
        desired_positions.at(0) = c2h;
        desired_positions.at(1) = -c2h;
//		desired_positions.at(0) = -1;
//		desired_positions.at(1) = 0;
        
        // 3. Update current positions
        current_positions.clear();
        current_positions.resize(2);
        //current_positions = complianceController->assignCurrentVector(simRos->jointPositions);
        current_positions.at(0) = simRos->jointPositions[6];
        current_positions.at(1) = simRos->jointPositions[12];
        
        // 4. Calculate and update current velocities 
        current_velocities.clear();
        current_velocities.resize(2);
        //current_velocities = complianceController->assignCurrentVector(simRos->jointVelocities);
        current_velocities.at(0) = simRos->jointVelocities[6];
        current_velocities.at(1) = simRos->jointVelocities[12];

        // 5. Calculate desired joint velocities
        desired_velocities.clear();
        desired_velocities.resize(2);
        float dx_cf = desired_positions.at(0) - prev_valueCF;
        float dx_ft = desired_positions.at(1) - prev_valueFT;
        float angular_velocity_cf = (float) dx_cf/dt;
        float angular_velocity_ft = (float) dx_ft/dt;
        desired_velocities.at(0) = angular_velocity_cf;
        desired_velocities.at(1) = angular_velocity_ft;

        // 6. ADAPTATION LAW: Calculate joint torques
        //vector<float> controlled_torque = complianceController->interpolateTorque(current_positions, desired_positions, current_velocities, desired_velocities); // Matrix K and D
        vector<float> controlled_torque = complianceController->approximateTorque(current_positions, desired_positions, current_velocities, desired_velocities); // Vector K and D

        
        //complianceController->printVector(controlled_torque, "Torque");
        //cout << "Torque on FT0 = " << controlled_torque[12] << endl;

        //--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--
        if(isSimulation == true)
        {       
            // 7A. Threshold max torque
            //vector<float> applied_torque;
            applied_torque.resize(controlled_torque.size());
            for(size_t i=0; i<controlled_torque.size(); i++)
            {
                if(controlled_torque[i] > MAX_TORQUE)
                {
                    applied_torque.at(i) = MAX_TORQUE;
                }
                else if(controlled_torque[i] < -MAX_TORQUE)
                {
                    applied_torque.at(i) = -MAX_TORQUE;
                }
                else
                {
                    applied_torque.at(i) = controlled_torque[i];
                }
            }

            
            //complianceController->printVector(applied_torque, "Torque MAX/MIN thresholded");
            //cout << endl << "-------------------------------------" << endl << endl;

            // 8A. Send motor torques
            
            int forward = -1;
            motor_values.at(BC0) = 0;//-MotorOutput4;
            motor_values.at(BC1) = 0;//-MotorOutput0Neg;
            motor_values.at(BC2) = 0;//-MotorOutput3;
            motor_values.at(BC3) = 0;//MotorOutput4Neg;
            motor_values.at(BC4) = 0;//-MotorOutput9;
            motor_values.at(BC5) = 0;//MotorOutput3Neg;

            motor_values.at(CF0) = -1;//applied_torque.at(0);//complianceController->fabs(controlled_torque[6]);
            motor_values.at(CF1) = -1;//complianceController->fabs(controlled_torque[7]);
            motor_values.at(CF2) = -1;//complianceController->fabs(controlled_torque[8]);
            motor_values.at(CF5) = -1;//complianceController->fabs(controlled_torque[9]);
            motor_values.at(CF4) = -1;//complianceController->fabs(controlled_torque[10]);
            motor_values.at(CF3) = -1;//complianceController->fabs(controlled_torque[11]);

            motor_values.at(FT0) = applied_torque.at(1);//desired_positions.at(1);
            motor_values.at(FT1) = 0;//FT_pos;//complianceController->fabs(controlled_torque[13]);
            motor_values.at(FT2) = 0;//1.4;//complianceController->fabs(controlled_torque[14]);
            motor_values.at(FT5) = 0;//1.4;//complianceController->fabs(controlled_torque[15]);
            motor_values.at(FT4) = 0;//FT_pos;//complianceController->fabs(controlled_torque[16]);
            motor_values.at(FT3) = 0;//0.6;//complianceController->fabs(controlled_torque[17]);

            // Control Errors
            float et = current_positions[1] - desired_positions[1]; 
            motor_values.at(18) = et;
            float eet = current_velocities[1] - desired_velocities[1];
            motor_values.at(19) = eet;

            // Back-bone
            motor_values.at(20) = 0.0;
            motor_values.at(21) = 1.0;
            //motor_values.at(20) = actual_torque_error;
            //motor_values.at(21) = optB;


            //cout << "Beta: " << optB << endl;
            //cout << "Torque error:" << actual_torque_error << endl;
            cout << endl << "-------------------------------------" << endl << endl;  
        
            // 9A. Save data
            if (savings < 2000)
            { 
                // Motor Commands
                // AMC Data: simTime, torque, torque_feedback, desired_position, position_feedback, desired_velocity, velocity_feedback
                AMCData << simRos->simulationTime << "," << applied_torque[0] << "," << simRos->jointTorques[6] << "," << applied_torque[1] << "," << simRos->jointTorques[12] << "," << desired_positions[0] << "," << simRos->jointPositions[6] << "," << desired_positions[1] << "," << simRos->jointPositions[12] << "," << desired_velocities[0] << "," << simRos->jointVelocities[6] << "," << desired_velocities[1] << "," << simRos->jointVelocities[12] << endl;
                logTCData(simRos->simulationTime, simRos->jointTorques[6], controlled_torque[0], applied_torque[0], simRos->jointPositions[6], desired_positions[0], simRos->jointVelocities[6], desired_velocities[0], dt);
            }

            // 10. Update Control Values
            previous_simTime = current_simTime;
            previous_positionCF = current_positions[0]; // real position
            previous_positionFT = current_positions[1];
            prev_valueCF = desired_positions.at(0); // ideal position
            prev_valueFT = desired_positions.at(1);
        }
        //--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--sim--
        else
        {
            // 7B. Threshold max torque
            float applied_torque;
            if(controlled_torque[12] > MAX_TORQUE)
            {
                applied_torque = MAX_TORQUE;
            }
            else if(controlled_torque[12] < -MAX_TORQUE)
            {
                applied_torque = -MAX_TORQUE;
            }
            else
            {
                applied_torque = controlled_torque[12];
            }

            // 8B. Send motor torques
            int forward = -1;
            motor_values.at(BC0) = 0;//controlled_torque[0];
            motor_values.at(BC1) = 0;//controlled_torque[1];
            motor_values.at(BC2) = 0;//controlled_torque[2];
            motor_values.at(BC3) = 0;//controlled_torque[3];
            motor_values.at(BC4) = 0;//controlled_torque[4];
            motor_values.at(BC5) = 0;//controlled_torque[5];

            motor_values.at(CF0) = controlled_torque[6];
            motor_values.at(CF1) = 0;//controlled_torque[7];
            motor_values.at(CF2) = 0;//controlled_torque[8];
            motor_values.at(CF5) = 0;//controlled_torque[9];
            motor_values.at(CF4) = 0;//controlled_torque[10];
            motor_values.at(CF3) = 0;//controlled_torque[11];

            motor_values.at(FT0) = controlled_torque[12];
            motor_values.at(FT1) = 0;//controlled_torque[13];
            motor_values.at(FT2) = 0;//controlled_torque[14];
            motor_values.at(FT5) = 0;//controlled_torque[15];
            motor_values.at(FT4) = 0;//controlled_torque[16];
            motor_values.at(FT3) = 0;//controlled_torque[17];

            // Raw CPG values 
            motor_values.at(18) = _o1_cpg;
            motor_values.at(19) = _o2_cpg;

            // Back-bone
            //motor_values.at(20) = 0.0;
            //motor_values.at(21) = 1.0;
            motor_values.at(20) = actual_torque_error;
            motor_values.at(21) = optB;

            // 9B. Save data
            if (savings < 2000)
            { 
                // Motor Commands
                logTCData(simRos->simulationTime, simRos->jointTorques[12], controlled_torque[12], applied_torque, simRos->jointPositions[12], desired_positions[12], simRos->jointVelocities[12], desired_velocities[12], dt);
            }

            // 10. Update Control Values
            previous_simTime = current_simTime;
            previous_positionFT = current_positions[12]; // real position
            prev_valueFT = desired_positions.at(12); // ideal position
        }

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

    }

    // Send vector with motor values for actuation
    simRos->actuateRobotMotors(motor_values);

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

    // POST ACTUATION
    savings = savings + 1;

    for (auto &i : tauDelayLine)
        i.Step();

    for (auto &i : tauLeftDelayLine)
        i.Step();

    simRos->plot(data);
}
