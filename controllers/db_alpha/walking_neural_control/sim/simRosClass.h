//
// Edited by Carlos on 27/02/2019.
// From Neutron Modular Controller by Mathias Thor.
//

#ifndef DUNG_BEETLE_CONTROLLER_SIMROSCLASS_H
#define DUNG_BEETLE_CONTROLLER_SIMROSCLASS_H

#include <cstdio>
#include <cstdlib>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include <random>
#include <iostream>


class simRosClass 
{
    private:

        // ROS Topics
        std::string MotorTopic;
        //std::string MotorTorqueTopic;
        std::string simulationTimeTopic;
        std::string terminateNodeTopic;
        std::string startSimTopic;
        std::string pauseSimTopic;
        std::string stopSimTopic;
        std::string enableSyncModeTopic;
        std::string triggerNextStepTopic;
        std::string simulationStepDoneTopic;
        std::string simulationStateTopic;
        std::string CPGOutputTopic;
        std::string jointPositionTopic;
        std::string testParametersTopic;
        std::string jointTorqueTopic;
        std::string jointVelocityTopic;
        std::string jointLPFtorqueTopic;
        std::string controllerTypeTopic;


        // ROS Subscribers
        ros::Subscriber subSimulationTimeSub;
        ros::Subscriber subTerminateNodeSub;
        ros::Subscriber simulationStepDoneSub;
        ros::Subscriber simulationStateSub;
        ros::Subscriber jointPositionSub;
        ros::Subscriber testParametersSub;
        ros::Subscriber jointTorqueSub;
        ros::Subscriber jointVelocitySub;
        ros::Subscriber jointLPFtorqueSub;
        ros::Subscriber controllerTypeSub;

        // ROS Publishers
        ros::Publisher MotorActuatorPub;
        ros::Publisher CPGTopicPub;
        ros::Publisher startSimPub;
        ros::Publisher pauseSimPub;
        ros::Publisher stopSimPub;
        ros::Publisher enableSyncModePub;
        ros::Publisher triggerNextStepPub;

        // Private Global Variables
        struct timeval tv;
        __time_t currentTime_updatedByTopicSubscriber=0;
        bool simStepDone=false;
        ros::Rate* rate;
        int _counter=0;
        std::random_device myseed ;

        // Private Methods
        void simulationTimeCallback(const std_msgs::Float32& simTime);
        void terminateNodeCallback(const std_msgs::Bool& termNode);
        void simulationStepDoneCallback(const std_msgs::Bool& simStepDoneAns);
        void simulationStateCallback(const std_msgs::Int32& stateAns);
        void jointPositionCallback(const std_msgs::Float32MultiArray& jointPositions);
        void testParametersCallback(const std_msgs::Float32MultiArray& _testParameters);
        void jointTorqueCallback(const std_msgs::Float32MultiArray& _torqueVec);
        void jointVelocityCallback(const std_msgs::Float32MultiArray& _velVec);
        void jointLPFTorqueCallback(const std_msgs::Float32MultiArray& _torqueVec);
        void controllerTypeCallback(const std_msgs::Bool& _msg);

    public:

        // Constructor
        simRosClass(int argc, char *argv[]);
        
        // Destructor
        ~simRosClass();

        // Methods
        void plot(std::vector<float> data);
        void actuateRobotMotors(std::vector<float> values);
        //void setLegMotorTorques(std::vector<float> torques);
        void rosSpinOnce();
        void synchronousSimulation(unsigned char);
        void triggerSim();
        void triggerSimNoWait();
        bool getControllerType();

        // Public Global Variables
        bool controllerType;
        int simState=0;
        std::vector<float> testParameters={0,0,0,0,0,0,0};
        std::vector<float> jointPositions={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        std::vector<float> jointTorques={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        std::vector<float> jointLPFTorques={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        std::vector<float> jointVelocities={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        float simulationTime=0.0;
        bool terminateSimulation = false;
};


#endif //DUNG_BEETLE_CONTROLLER_SIMROSCLASS_H
