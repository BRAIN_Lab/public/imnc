//
// Created by Carlos on 12/02/19.
//

#include "signalPatterns.h"


//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

// SIGNAL GENERATION

// Phase inputed in degrees
float signalPatterns::sine(float amp, float f, double rad, float phase)
{
    float w = 2*PI*f;
    float phase_rad = (phase*2*PI)/360;
    float y = (float) amp*sin(w*rad + phase_rad);
    return y;
}

// Sine wave thresholded after reaching maximum
tuple<float, float> signalPatterns::thresholded_sine(float amp, float f, double rad, float z_prev, float phase)
{
    float w = 2*PI*f;
    float phase_rad = (phase*2*PI)/360;
    float y;
    float z = (float) amp*sin(w*rad + phase_rad);

    // First threshold, on F(x)
    if(z >= 0.0)
    {
        // Second threshold, on F'(x)
        float diff = z - z_prev;
        if(diff >= 0.0)
        {
            y = z;
        }
        else
        {
            y = 0.0;
        }
    }
    else
    {
        y = 0.0;
    }
    return make_tuple(y, z);
}

// Rectified sine wave extended with MAX-AMP constant for 1/4 period.
tuple<float, float> signalPatterns::quarter_positive(float amp, float f, double rad, float z_prev, float phase)
{
    float w = 2*PI*f;
    float phase_rad = (phase*2*PI)/360;
    float y;
    float offset = (90*2*PI)/360;
    float z = (float) amp*sin(w*rad + phase_rad);

    // First threshold, on F(x)
    if(z >= 0.0)
    {
        // Second threshold, on F'(x)
        float diff = z - z_prev;
        if(diff >= 0.0)
        {
            y = z;
        }
        else
        {
            y = amp;
        }
    }
    else
    {
        // Second filter applied:
        float diff = z - z_prev;
        if(diff <= 0.0)
        {
            y = (float) amp*sin(w*rad + (phase_rad - offset));
        }
        else
        {
            y = 0.0;
        }
        
    }
    return make_tuple(y, z);
}

// Half rectified sine wave
float signalPatterns::half_rectified_sine(float amp, float f, double rad, float phase)
{
    float w = 2*PI*f;
    float phase_rad = (phase*2*PI)/360;
    float z = (float) amp*sin(w*rad + phase_rad);
    float y;

    if(z >= 0.0)
    {
        y = z;
    }
    else
    {
        y = 0.0;
    }

    return y;
}


// Full rectified sine wave.
float signalPatterns::full_rectified_sine(float amp, float f, double rad, float phase)
{
    float w = 2*PI*f;
    float phase_rad = (phase*2*PI)/360;
    float z = (float) amp*sin(w*rad + phase_rad);
    float y;

    if(z >= 0.0)
    {
        y = z;
    }
    else
    {
        y = -z;
    }

    return y;
}

tuple<float,float> signalPatterns::quarter_negative(float amp, float f, double rad, float z_prev, float phase)
{
    float w = 2*PI*f;
    float phase_rad = (phase*2*PI)/360;
    float y;
    float z = (float) amp*sin(w*rad + phase_rad);

    // First threshold, on F(x)
    if(z >= 0.0)
    {
        y = z;
    }
    else
    {
        // Second filter applied:
        float diff = z - z_prev;
        if(diff <= 0.0)
        {
            y = z;
        }
        else
        {
            y = -amp;
        }
    }
    return make_tuple(y, z);
}

tuple<float,float> signalPatterns::rear_coxa(float amp, float f, double rad, float z_prev, float phase)
{
    float w = 2*PI*f;
    float phase_rad = (phase*2*PI)/360;
    float y;
    //float offset = (90*2*PI)/360;
    float z = (float) amp*sin(w*rad + phase_rad);

    // First threshold, on F(x)
    if(z >= 0.0)
    {
        // Second threshold, on F'(x)
        float diff = z - z_prev;
        if(diff >= 0.0)
        {
            y = z;
        }
        else
        {
            y = 0.0;
            /*float v = (float) amp*sin(w*rad + (phase_rad + offset));
            y = v/1.5;*/
        }
    }
    else
    {
        //y = -amp/1.5;
        // Second filter applied:
        float diff = z - z_prev;
        if(diff <= 0.0)
        {
            y = z/1.5;
        }
        else
        {
            y = -amp/1.5;
        }
    }
    return make_tuple(y, z);
}


//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

// SIGNAL FILTERS

// Only one half of the wave is propagated
float signalPatterns::half_rectification(float value, float min)
{
    float result;
    if(value >= min)
    {
        result = value;
    }
    else
    {
        result = min;
    }

    return result;
}

// Only one quarter of the wave is propagated
float signalPatterns::quarter_rectification(float value, float prev, float min)
{
    float result;
    if(value >= min)
    {
        float diff = value - prev;
        if(diff >= 0)
        {
            result = value;
        }
        else
        {
            result = min;
        }
        
    }
    else
    {
        result = min;
    }

    return result;
}

// Thre quarters of the wave are propagated
float signalPatterns::quarter_depression(float value, float prev, float min)
{
    float result;
    if(value <= min)
    {
        float diff = value - prev;
        if(diff >= 0)
        {
            result = min;
        }
        else
        {
            result = value;
        }
        
    }
    else
    {
        result = value;
    }

    return result;
}

// Last quarter of the wave remains constant at -amp
float signalPatterns::elongation(float value, float prev, float min, float amp)
{
    float result;
    if(value <= min)
    {
        float diff = value - prev;
        if(diff >= 0)
        {
            result = amp;
        }
        else
        {
            result = value;
        }
        
    }
    else
    {
        float diff = value - prev;
        if(diff >= 0)
        {
            result = value;
        }
        else
        {
            result = min;
        }
        
    }
    return result;
}