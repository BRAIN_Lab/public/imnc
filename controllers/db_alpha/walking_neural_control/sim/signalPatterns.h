// Created by mat on 12/02/19.


//#ifndef DUNG_BEETLE_CONTROLLER_SIGNALPATTERNS_H
//#define DUNG_BEETLE_CONTROLLER_SIGNALPATTERNS_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <tuple>
#include <vector>
#include <math.h>

#define PI 3.14159265359

using namespace std;

class signalPatterns
{
    public:

        // Public Methods

        // Signal generation patterns
        float sine(float amp, float f, double rad, float phase);
        tuple<float, float> thresholded_sine(float amp, float f, double rad, float z_prev, float phase);
        tuple<float, float> quarter_positive(float amp, float f, double rad, float z_prev, float phase);
        float half_rectified_sine(float amp, float f, double rad, float phase);
        float full_rectified_sine(float amp, float f, double rad, float phase);
        tuple<float, float> quarter_negative(float amp, float f, double rad, float z_prev, float phase);
        tuple<float, float> rear_coxa(float amp, float f, double rad, float z_prev, float phase);

        // Signal filters
        float half_rectification(float value, float min);
        float quarter_rectification(float value, float prev, float min);
        float quarter_depression(float value, float prev, float min);
        float elongation(float value, float prev, float min, float amp);
};

//#endif //DUNG_BEETLE_CONTROLLER_SIGNALPATTERNS_H