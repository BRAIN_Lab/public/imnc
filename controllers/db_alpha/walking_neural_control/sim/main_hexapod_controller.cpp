/*
 * Written by Mathias Thor DEC 30
 * Happy NewYear!
 */

#include "dungBeetleController.h"

// Main code:
int main(int argc,char* argv[])
{
    dungBeetleController controller(argc,argv);
    while(controller.runController()){}
    return(0);
}
