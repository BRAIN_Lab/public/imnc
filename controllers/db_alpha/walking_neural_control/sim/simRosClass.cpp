//
// Edited by Carlos on 27/02/2019.
// From Neutron Modular Controller by Mathias Thor.
//

#include "simRosClass.h"


// Constructor
simRosClass::simRosClass(int argc, char **argv) 
{
    if (argc>17)
    {
        // Get topic names
        MotorTopic=argv[1];
        simulationTimeTopic=argv[2];
        terminateNodeTopic=argv[3];
        //std::cout << terminateNodeTopic << std::endl;
        startSimTopic=argv[4];
        pauseSimTopic=argv[5];
        stopSimTopic=argv[6];
        enableSyncModeTopic=argv[7];
        //std::cout << enableSyncModeTopic << std::endl;
        triggerNextStepTopic=argv[8];
        //std::cout << triggerNextStepTopic << std::endl;
        simulationStepDoneTopic=argv[9];
        simulationStateTopic=argv[10];
        CPGOutputTopic=argv[11];
        jointPositionTopic=argv[12];
        testParametersTopic=argv[13];
        jointTorqueTopic=argv[14];
        jointLPFtorqueTopic=argv[15];
        jointVelocityTopic=argv[16];
        controllerTypeTopic=argv[17];

        // Extend topic names
        MotorTopic="/"+MotorTopic;
        //MotorTorqueTopic="/"+MotorTorqueTopic;
        simulationTimeTopic="/"+simulationTimeTopic;
        terminateNodeTopic="/"+terminateNodeTopic;
        startSimTopic="/"+startSimTopic;
        pauseSimTopic="/"+pauseSimTopic;
        stopSimTopic="/"+stopSimTopic;
        enableSyncModeTopic="/"+enableSyncModeTopic;
        triggerNextStepTopic="/"+triggerNextStepTopic;
        simulationStepDoneTopic="/"+simulationStepDoneTopic;
        simulationStateTopic="/"+simulationStateTopic;
        CPGOutputTopic="/"+CPGOutputTopic;
        jointPositionTopic="/"+jointPositionTopic;
        testParametersTopic="/"+testParametersTopic;
        jointTorqueTopic="/"+jointTorqueTopic;
        jointLPFtorqueTopic="/"+jointLPFtorqueTopic;
        jointVelocityTopic="/"+jointVelocityTopic;
        controllerTypeTopic="/"+controllerTypeTopic;
    }
    else
    {
        ROS_ERROR("Not enough arguments for C++ simROS node!\n");
        sleep(5000);
        return;
    }

    // Create a ROS nodes (The name has a random component) ---> Add Controller Type to the node name
    int _argc = 0;
    char** _argv = NULL;
    if (gettimeofday(&tv, NULL)==0)
        currentTime_updatedByTopicSubscriber=(tv.tv_sec*1000+tv.tv_usec/1000)&0x00ffffff;
    std::string nodeName("simROS");
    std::string randId(boost::lexical_cast<std::string>(currentTime_updatedByTopicSubscriber+int(999999.0f*(rand()/(float)RAND_MAX))));
    nodeName+=randId;
    ros::init(_argc,_argv,nodeName);

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("simROS just started!");

    // Subscribe to topics and specify callback functions
    subSimulationTimeSub=node.subscribe(simulationTimeTopic, 1, &simRosClass::simulationTimeCallback, this);
    subTerminateNodeSub=node.subscribe(terminateNodeTopic, 1, &simRosClass::terminateNodeCallback, this);
    simulationStepDoneSub=node.subscribe(simulationStepDoneTopic, 1, &simRosClass::simulationStepDoneCallback, this);
    simulationStateSub=node.subscribe(simulationStateTopic, 1, &simRosClass::simulationStateCallback, this);
    jointPositionSub=node.subscribe(jointPositionTopic, 1, &simRosClass::jointPositionCallback, this);
    testParametersSub=node.subscribe(testParametersTopic, 1, &simRosClass::testParametersCallback, this);
    jointTorqueSub=node.subscribe(jointTorqueTopic, 1, &simRosClass::jointTorqueCallback, this);
    jointLPFtorqueSub=node.subscribe(jointLPFtorqueTopic, 1, &simRosClass::jointLPFTorqueCallback, this);
    jointVelocitySub=node.subscribe(jointVelocityTopic, 1, &simRosClass::jointVelocityCallback, this);
    controllerTypeSub=node.subscribe(controllerTypeTopic, 1, &simRosClass::controllerTypeCallback, this);

    // Initialize publishers
    MotorActuatorPub=node.advertise<std_msgs::Float32MultiArray>(MotorTopic,1);
    //MotorTorquePub=node.advertise<std_msgs::Float32MultiArray>(MotorTorqueTopic,1);
    CPGTopicPub=node.advertise<std_msgs::Float32MultiArray>(CPGOutputTopic,1);
    startSimPub=node.advertise<std_msgs::Bool>(startSimTopic,1);
    pauseSimPub=node.advertise<std_msgs::Bool>(pauseSimTopic,1);
    stopSimPub=node.advertise<std_msgs::Bool>(stopSimTopic,1);
    enableSyncModePub=node.advertise<std_msgs::Bool>(enableSyncModeTopic,1);
    triggerNextStepPub=node.advertise<std_msgs::Bool>(triggerNextStepTopic,1);

    // rate = new ros::Rate(22); // was 25
}

//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

// PRIVATE METHODS

// Simulation time callback
void simRosClass::simulationTimeCallback(const std_msgs::Float32& simTime)
{
    simulationTime=simTime.data;
}

// Terminate Node Callback
void simRosClass::terminateNodeCallback(const std_msgs::Bool& termNode)
{
    terminateSimulation=termNode.data;
}


// Controller Type defined in LUA interface
void simRosClass::controllerTypeCallback(const std_msgs::Bool& _msg)
{
    controllerType = _msg.data;
}


// Time step callback
void simRosClass::simulationStepDoneCallback(const std_msgs::Bool& _simStepDone)
{
    simStepDone=_simStepDone.data;
}

// Simulation state callback
void simRosClass::simulationStateCallback(const std_msgs::Int32& _state)
{
    simState=_state.data;
}

// Joint Positions callback
void simRosClass::jointPositionCallback(const std_msgs::Float32MultiArray& _jointPositions)
{
    //ROS_INFO("The BC position is: %f", _jointPositions.data[0]);
    jointPositions = _jointPositions.data;
}


// Test Parameters Callback
void simRosClass::testParametersCallback(const std_msgs::Float32MultiArray& _testParameters)
{
    testParameters = _testParameters.data;
}


// Joint Torque Vector callback
void simRosClass::jointTorqueCallback(const std_msgs::Float32MultiArray& _torqueVec)
{
    jointTorques = _torqueVec.data;
}


// Joint Velocity Vector callback
void simRosClass::jointVelocityCallback(const std_msgs::Float32MultiArray& _velVec)
{
    jointVelocities = _velVec.data;
}


// LPF Joint Torque Vector callback
void simRosClass::jointLPFTorqueCallback(const std_msgs::Float32MultiArray& _torqueVec)
{
    jointLPFTorques = _torqueVec.data;
}

//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-


// PUBLIC METHODS


// Get controller type: Position Control (true) or Torque control (false)
bool simRosClass::getControllerType()
{
    bool type = controllerType;
    return type;
}

// Set Motor Positions
void simRosClass::actuateRobotMotors(std::vector<float> values) 
{
    // publish the motor positions:
    std_msgs::Float32MultiArray array;
    array.data.clear();
    for (size_t i = 0; i < values.size(); ++i) 
    {
        array.data.push_back(values[i]);
    }
    MotorActuatorPub.publish(array);
}

// Plot data
void simRosClass::plot(std::vector<float> data)
{
    // publish the motor positions:
    std_msgs::Float32MultiArray array;
    array.data.clear();

    for (int i = 0; i <= data.size(); ++i)
        array.data.push_back(data[i]);

    CPGTopicPub.publish(array);
}


// RosSpinOnce call
void simRosClass::rosSpinOnce()
{
    ros::spinOnce();
}


// Trigger simulation
void simRosClass::triggerSim()
{
    std_msgs::Bool _bool;
    _bool.data = static_cast<unsigned char>(true);

    triggerNextStepPub.publish(_bool);
    rosSpinOnce();

    int stuckTester = 0;
    while(_counter == testParameters[11]){
        rosSpinOnce();
        stuckTester++;

        if(stuckTester > 20000000 && simulationTime > 10){
            ROS_INFO("Simulation did not step.");
            _counter = testParameters[11];
            return;
        }
    }

    if (_counter+1 != testParameters[11])
        ROS_INFO("Simulation advanced two or more steps in one controller step");

    _counter = testParameters[11];
}


// Trigger simulation counter
void simRosClass::triggerSimNoWait()
{
    std_msgs::Bool _bool;
    _bool.data = static_cast<unsigned char>(true);
    triggerNextStepPub.publish(_bool);
    rosSpinOnce();
    _counter = testParameters[11];
}


// Synchronous simulation
void simRosClass::synchronousSimulation(unsigned char boolean)
{
    std_msgs::Bool _bool;
    _bool.data = boolean;
    enableSyncModePub.publish(_bool);

    //ros::spinOnce();
}


//+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-


// Destructor
simRosClass::~simRosClass()
{
    ROS_INFO("simROS just terminated!");
    ros::shutdown();
}
