-- VREP-ROS-C++ CONTROLLER INTERFACE FOR SIMULATION 

--[[ Callback function for reciving motor positions over ROS --]]

--[[ CONSTANTS --]]
math.randomseed(os.time())
varianceMotor = 0.000
varianceSensor = 0.000;

-- SET UP POSITION / TORQUE CONTROL MODE 
--PorT=false -- Closed-loop Torque Control (muscle model) implementing the Adaptive Motor Controller
PorT=true -- Closed-Loop CPG-driven Position PID Controller adapted for the dung beetle tripod gait.

-- Function to round floats to a fixed decimal number
local function roundToNthDecimal(num, n)
    local mult = 10^(n or 0)
    return math.floor(num * mult + 0.5) / mult
end

-- Implementation of the Gaussian function
function gaussian (mean, variance)
    return  math.sqrt(-2 * variance * math.log(math.random())) * math.cos(2 * math.pi * math.random()) + mean
    --return mean+math.random()*variance
end

function simGetJointVelocity (jointHandle)
    res,velocity=simGetObjectFloatParameter(jointHandle,2012)
    return  velocity
end


-- Get the joint's positional difference (in angle)
getAngleMinusAlpha=function(angle,alpha)
    local sinAngle0=math.sin(angle)
    local sinAngle1=math.sin(alpha)
    local cosAngle0=math.cos(angle)
    local cosAngle1=math.cos(alpha)
    local sin_da=sinAngle0*cosAngle1-cosAngle0*sinAngle1
    local cos_da=cosAngle0*cosAngle1+sinAngle0*sinAngle1
    return(math.atan2(sin_da,cos_da))
end

-- IDK
function sma(period)
    local t = {}
    function sum(a, ...)
        if a then return a+sum(...) else return 0 end
    end
    function average(n)
        if #t == period then table.remove(t, 1) end
        t[#t + 1] = n
        return sum(unpack(t)) / #t
    end
    return average
end


-- POSITION CONTROL CALLBACK
-- Set the motors to the target positions (vector positions)
function actuateMotors_cb(msg)

    data = msg.data
    --sim.setJointTargetPosition(TC_motor1,data[7])
    --sim.setJointTargetPosition(CF_motor1,data[8])
    --sim.setJointTargetPosition(FT_motor1,-data[9])

    sim.setJointTargetPosition(TC_motor0,data[13])
    sim.setJointTargetPosition(TC_motor1,data[7])
    sim.setJointTargetPosition(TC_motor2,data[1])
    sim.setJointTargetPosition(TC_motor3,data[16])
    sim.setJointTargetPosition(TC_motor4,data[10])
    sim.setJointTargetPosition(TC_motor5,data[4])
    sim.setJointTargetPosition(CF_motor0,data[14])
    sim.setJointTargetPosition(CF_motor1,data[8])
    sim.setJointTargetPosition(CF_motor2,data[2])
    sim.setJointTargetPosition(CF_motor3,data[17])
    sim.setJointTargetPosition(CF_motor4,data[11])
    sim.setJointTargetPosition(CF_motor5,data[5])
    sim.setJointTargetPosition(FT_motor0,-data[15])
    sim.setJointTargetPosition(FT_motor1,-data[9])
    sim.setJointTargetPosition(FT_motor2,-data[3])
    sim.setJointTargetPosition(FT_motor3,-data[18])
    sim.setJointTargetPosition(FT_motor4,-data[12])
    sim.setJointTargetPosition(FT_motor5,-data[6])
    sim.setJointTargetPosition(TA_motor,data[20])
    --testParameters[1]=data[19] --TODO
    --testParameters[4]=data[20] --TODO
end

-- TORQUE CONTROL CALLBACK
-- Set the motors to the target Torques (vector torques)
-- Timer function???????
function setMotorTorques_cb(msg)
    data = msg.data
    
    -- 1. Set target velocities to unreachable values
    -- 2. Limit maximum Torque to modulate the behavior of the joint
    
    -- Wait 5 seconds until controller starts
    tm = simGetSimulationTime()
    if (tm < 3) then
        
        sim.setJointTargetPosition(TC_motor0,0)
        sim.setJointTargetPosition(TC_motor1,0)
        sim.setJointTargetPosition(TC_motor2,0)
        sim.setJointTargetPosition(TC_motor3,0)
        sim.setJointTargetPosition(TC_motor4,0)
        sim.setJointTargetPosition(TC_motor5,0)

        sim.setJointTargetPosition(CF_motor0,-1)
        sim.setJointTargetVelocity(CF_motor0,0)
        sim.setJointForce(CF_motor0,math.abs(0))
        
        sim.setJointTargetPosition(CF_motor1,-1)
        sim.setJointTargetPosition(CF_motor2,-1)
        sim.setJointTargetPosition(CF_motor3,-1)
        sim.setJointTargetPosition(CF_motor4,-1)
        sim.setJointTargetPosition(CF_motor5,-1)
        
        sim.setJointTargetPosition(FT_motor0,0)
        sim.setJointTargetVelocity(FT_motor0,0)
        sim.setJointForce(FT_motor0,math.abs(0))
        
        sim.setJointTargetPosition(FT_motor1,0)
        sim.setJointTargetPosition(FT_motor2,0)
        sim.setJointTargetPosition(FT_motor3,0)
        sim.setJointTargetPosition(FT_motor4,0)
        sim.setJointTargetPosition(FT_motor5,0)

    else

        -- TC Joints (No torque control)
        sim.setJointTargetPosition(TC_motor0,data[13])
        sim.setJointTargetPosition(TC_motor1,data[7])
        sim.setJointTargetPosition(TC_motor2,data[1])
        sim.setJointTargetPosition(TC_motor3,data[16])
        sim.setJointTargetPosition(TC_motor4,data[10])
        sim.setJointTargetPosition(TC_motor5,data[4])
        
        -- CF0
        -- Joint Velocity
        if (data[14] < 0) then
            sim.setJointTargetVelocity(CF_motor0, 90000)   -- Unreacheable low number
        elseif (data[14] == 0) then
            sim.setJointTargetVelocity(CF_motor0, 0)
        else
            sim.setJointTargetVelocity(CF_motor0, -90000)    -- Unreacheable high number
        end
        -- Joint Torque
        if(simGetSimulationTime()>-1) then
            sim.setJointForce(CF_motor0,math.abs(data[14]))
        else
            sim.setJointForce(CF_motor0,math.abs(0))
        end
        
        -- FT0
        -- Joint Velocity
        if (data[15] < 0) then
            sim.setJointTargetVelocity(FT_motor0, 90000)   -- Unreacheable low number
        elseif (data[15] == 0) then
            sim.setJointTargetVelocity(FT_motor0, 0)
        else
            sim.setJointTargetVelocity(FT_motor0, -90000)    -- Unreacheable high number
        end
        -- Joint Torque
        if(simGetSimulationTime()>-1) then
            sim.setJointForce(FT_motor0,math.abs(data[15]))
        else
            sim.setJointForce(FT_motor0,math.abs(0))
        end

        -- Joint Positions
        --sim.setJointTargetPosition(CF_motor0,data[14])
        sim.setJointTargetPosition(CF_motor1,data[8])
        sim.setJointTargetPosition(CF_motor2,data[2])
        sim.setJointTargetPosition(CF_motor3,data[17])
        sim.setJointTargetPosition(CF_motor4,data[11])
        sim.setJointTargetPosition(CF_motor5,data[5])
        --sim.setJointTargetPosition(FT_motor0,data[15])
        sim.setJointTargetPosition(FT_motor1,data[9])
        sim.setJointTargetPosition(FT_motor2,data[3])
        sim.setJointTargetPosition(FT_motor3,data[18])
        sim.setJointTargetPosition(FT_motor4,data[12])
        sim.setJointTargetPosition(FT_motor5,data[6])
    end
end

--[[
Callback function for displaying data on a graph
--]]
function graph_cb(msg)
    data = msg.data
    simSetGraphUserData(graphHandle,"O1",data[1])
    simSetGraphUserData(graphHandle,"O2",data[2])
   -- simSetGraphUserData(graphHandle,"O3",data[3])
   -- simSetGraphUserData(graphHandle,"O4",data[4])
   -- simSetGraphUserData(graphHandle,"O5",data[5])
end

--[[
Function for handling slider changes on the UI
--]]
function sliderChange(ui,id,newVal)
    if id == 10+0 then
        newVal = newVal
        simUI.setLabelText(ui,1000+0,'Value: '..newVal)
        testParameters[1] = newVal
    elseif id == 10+1 then
        newVal = newVal/1000
        simUI.setLabelText(ui,1000+1,'Value: '..newVal)
        testParameters[2] = newVal/1000
    elseif id == 10+2 then
        newVal = newVal/1000
        simUI.setLabelText(ui,1000+2,'Value: '..newVal)
        testParameters[3] = newVal
    elseif id == 10+3 then
        newVal = newVal/1000
        simUI.setLabelText(ui,1000+3,'Value: '..newVal)
        testParameters[4] = newVal
    end
end

--[[
Initialization: Called once at the start of a simulation
--]]
if (sim_call_type==sim.childscriptcall_initialization) then

    stepCounter = 0;

    smaAN = sma(200);
    smaTO = sma(200);

    -- Low Pass Filtered Torques
    LPFjointTorque = 0;
    LPFjointTorqueTC0 = 0;
    LPFjointTorqueTC1 = 0;
    LPFjointTorqueTC2 = 0;
    LPFjointTorqueTC3 = 0;
    LPFjointTorqueTC4 = 0;
    LPFjointTorqueTC5 = 0;
    LPFjointTorqueCF0 = 0;
    LPFjointTorqueCF1 = 0;
    LPFjointTorqueCF2 = 0;
    LPFjointTorqueCF3 = 0;
    LPFjointTorqueCF4 = 0;
    LPFjointTorqueCF5 = 0;
    LPFjointTorqueFT0 = 0;
    LPFjointTorqueFT1 = 0;
    LPFjointTorqueFT2 = 0;
    LPFjointTorqueFT3 = 0;
    LPFjointTorqueFT4 = 0;
    LPFjointTorqueFT5 = 0;
  
    -- Low Pass Filtered Angular Velocities
    LPFAngVel = 0;

    -- User Interface setup
    xml = [[ <ui closeable="false" resizable="true" style="plastique" title="DB-ALPHA Controller Interface">
    <label text="AMC Parameter a" wordwrap="true" />
    <label text="Value: 0" id="1000" wordwrap="true" />
    <hslider id="10" tick-position="both-sides" tick-interval="1" minimum="0" maximum="5000" onchange="sliderChange" style="plastique" />

    <label text="AMC Parameter b" wordwrap="true" />
    <label text="Value: 0" id="1001" wordwrap="true" />
    <hslider id="11" tick-position="both-sides" tick-interval="1" minimum="0" maximum="50000" onchange="sliderChange" style="plastique" />

    <label text="CPG frequency value MI" wordwrap="true" />
    <label text="Value: 0" id="1002" wordwrap="true" />
    <hslider id="12" tick-position="both-sides" tick-interval="1" minimum="0" maximum="100" onchange="sliderChange" style="plastique" />

    <label text="AMC Parameter beta" wordwrap="true" />
    <label text="Value: 0" id="1003" wordwrap="true" />
    <hslider id="13" tick-position="both-sides" tick-interval="1" minimum="0" maximum="1000" onchange="sliderChange" style="plastique" />
    </ui> ]]

    -- Create User Interface
    ui=simUI.create(xml)

    -- Initialize sliders
    testParameters = {35, 0.03, 0.01, 0.05, 0, 0, 0}
    --simExtCustomUI_setSliderValue(ui,10,testParameters[1]*1000)
    simExtCustomUI_setSliderValue(ui,10,testParameters[1])
    --sliderChange(ui,10,testParameters[1]*1000)
    sliderChange(ui,10,testParameters[1])
    simExtCustomUI_setSliderValue(ui,11,testParameters[2])
    sliderChange(ui,11,testParameters[2])
    simExtCustomUI_setSliderValue(ui,12,testParameters[3]*1000)
    sliderChange(ui,12,testParameters[3]*1000)
    simExtCustomUI_setSliderValue(ui,13,testParameters[4]*1000)
    sliderChange(ui,13,testParameters[4]*1000)

    -- Set position of the User Interface
    x, y=simExtCustomUI_getPosition(ui)
    simExtCustomUI_setPosition(ui, x+800, y, true)

--    -- Create Controle (Can be written to with print('message')
--    if simGetScriptExecutionCount() == 0 then
--        size = {400,400}
--        pos = {0+140+(x+1920/2),1080-size[2]-800}
--        console = simAuxiliaryConsoleOpen("Aux Console", 500, 0x10, pos, size, NULL, NULL)
--        oldprint = print
--        print = function(...)
--            simAuxiliaryConsolePrint(console, ...)
--        end
--    end

    -- Create all handles
--[[
    robotHandle=sim.getObjectAssociatedWithScript(sim.handle_self)

    TC_motor0=sim.getObjectHandle("TC0")    -- Handle of the TC motor
    TC_motor1=sim.getObjectHandle("TC1")    -- Handle of the TC motor
    TC_motor2=sim.getObjectHandle("TC2")    -- Handle of the TC motor
    TC_motor3=sim.getObjectHandle("TC3")    -- Handle of the TC motor
    TC_motor4=sim.getObjectHandle("TC4")    -- Handle of the TC motor
    TC_motor5=sim.getObjectHandle("TC5")    -- Handle of the TC motor

    CF_motor0=sim.getObjectHandle("CF0")    -- Handle of the CF motor
    CF_motor1=sim.getObjectHandle("CF1")    -- Handle of the CF motor
    CF_motor2=sim.getObjectHandle("CF2")    -- Handle of the CF motor
    CF_motor3=sim.getObjectHandle("CF3")    -- Handle of the CF motor
    CF_motor4=sim.getObjectHandle("CF4")    -- Handle of the CF motor
    CF_motor5=sim.getObjectHandle("CF5")    -- Handle of the CF motor

    FT_motor0=sim.getObjectHandle("FT0")    -- Handle of the FT motor
    FT_motor1=sim.getObjectHandle("FT1")    -- Handle of the FT motor
    FT_motor2=sim.getObjectHandle("FT2")    -- Handle of the FT motor
    FT_motor3=sim.getObjectHandle("FT3")    -- Handle of the FT motor
    FT_motor4=sim.getObjectHandle("FT4")    -- Handle of the FT motor
    FT_motor5=sim.getObjectHandle("FT5")    -- Handle of the FT motor
--]]
    -- Create all handles
    robotHandle=sim.getObjectAssociatedWithScript(sim.handle_self)
    TC_motor0=sim.getObjectHandle("Thorax_Left_Joint1")      		-- Handle of the BC motor
    TC_motor1=sim.getObjectHandle("Leg_Abdomen_MidLeft_Joint1")      	-- Handle of the BC motor
    TC_motor2=sim.getObjectHandle("Leg_Abdomen_RearLeft_Joint1")      	-- Handle of the BC motor
    TC_motor3=sim.getObjectHandle("Thorax_Right_Joint1")      		-- Handle of the BC motor
    TC_motor4=sim.getObjectHandle("Leg_Abdomen_MidRight_Joint1")      	-- Handle of the BC motor
    TC_motor5=sim.getObjectHandle("Leg_Abdomen_RearRight_Joint1")      	-- Handle of the BC motor

    CF_motor0=sim.getObjectHandle("Thorax_Left_Joint2")     		-- Handle of the CF motor
    CF_motor1=sim.getObjectHandle("Abdomen_MidLeft_Joint2")     	-- Handle of the CF motor
    CF_motor2=sim.getObjectHandle("Abdomen_RearLeft_Joint2")     	-- Handle of the CF motor
    CF_motor3=sim.getObjectHandle("Thorax_Right_Joint2")     		-- Handle of the CF motor
    CF_motor4=sim.getObjectHandle("Abdomen_MidRight_Joint2")     	-- Handle of the CF motor
    CF_motor5=sim.getObjectHandle("Abdomen_RearRight_Joint2")     	-- Handle of the CF motor

    FT_motor0=sim.getObjectHandle("Thorax_Left_Joint3")    		-- Handle of the FT motor
    FT_motor1=sim.getObjectHandle("Abdomen_MidLeft_Joint3")    		-- Handle of the FT motor
    FT_motor2=sim.getObjectHandle("Abdomen_RearLeft_Joint3")    	-- Handle of the FT motor
    FT_motor3=sim.getObjectHandle("Thorax_Right_Joint3")    		-- Handle of the FT motor
    FT_motor4=sim.getObjectHandle("Abdomen_MidRight_Joint3")    	-- Handle of the FT motor
    FT_motor5=sim.getObjectHandle("Abdomen_RearRight_Joint3")    	-- Handle of the FT motor

    TA_motor = sim.getObjectHandle("AbdomenThorax_SpineJoint")    	-- Handle of the TA motor

    graphHandle=sim.getObjectHandle("Graph")                     -- Graph Handle
    papervisual=sim.getObjectHandle("Paper_Visualize")		 -- paper visualize graph handle
    
    dummesvin=sim.getObjectHandle("dummy")

    -- Handles real robot position?????
    previousPos=simGetJointPosition(FT_motor0)
    previousTime=0

    morfHexapod=sim.getObjectHandle("Alpha_Base")
    graphHandle=sim.getObjectHandle("Graph")                     -- Graph Handle

    -- Check if the required ROS plugin is loaded
    moduleName=0
    moduleVersion=0
    index=0
    pluginNotFound=true
    while moduleName do
        moduleName,moduleVersion=sim.getModuleName(index)
        if (moduleName=='RosInterface') then
            pluginNotFound=false
        end
        index=index+1
    end
    if (pluginNotFound) then
        sim.displayDialog('Error','The RosInterface was not found.',sim.dlgstyle_ok,false,nil,{0.8,0,0,0,0,0},{0.5,0,0,1,1,1})
    end

    -- If found then start the subscribers and publishers
    if (not pluginNotFound) then
	
        local MotorTopicName
        if (PorT==true) then
                MotorTopicName='MotorPositions'
        else
                MotorTopicName='MotorTorques'
        end

        local simulationTimeTopicName='simulationTime'
        local startSimulationName='startSimulation'
        local pauseSimulationName='pauseSimulation'
        local stopSimulationName='stopSimulation'
        local enableSyncModeName='enableSyncMode'
        local triggerNextStepName='triggerNextStep'
        local simulationStepDoneName='simulationStepDone'
        local simulationStateName='simulationState'
        local terminateControllerName='terminateController'
        local CPGOutputName='CPGOutput'
        local jointPositionsName='jointPositions'
        local testParametersName='testParameters'
	    local jointTorquesName='jointTorques'
        local jointLPFtorquesName='jointTorquesLPF'
        local jointVelocityName='jointVelocities'
        local controllerTypeName='ControllerType'
        local SimulationType='legged'

        -- Create the subscribers
        if (PorT==true) then
             MotorSub=simROS.subscribe('/'..MotorTopicName,'std_msgs/Float32MultiArray','actuateMotors_cb')
	    else
            MotorSub=simROS.subscribe('/'..MotorTopicName,'std_msgs/Float32MultiArray','setMotorTorques_cb')
	    end        
    
        CPGOutputSub=simROS.subscribe('/'..CPGOutputName,'std_msgs/Float32MultiArray', 'graph_cb')

        -- Create the publishers
        terminateControllerPub=simROS.advertise('/'..terminateControllerName,'std_msgs/Bool')
	    controllerTypePub=simROS.advertise('/'..controllerTypeName,'std_msgs/Bool')
        jointPositionsPub=simROS.advertise('/'..jointPositionsName,'std_msgs/Float32MultiArray')
        clockPub=simROS.advertise('/clock','rosgraph_msgs/Clock')
        testParametersPub=simROS.advertise('/'..testParametersName,'std_msgs/Float32MultiArray')
        jointTorquePub=simROS.advertise('/'..jointTorquesName,'std_msgs/Float32MultiArray')
        jointLPFtorquePub=simROS.advertise('/'..jointLPFtorquesName,'std_msgs/Float32MultiArray')
        jointVelocityPub=simROS.advertise('/'..jointVelocityName,'std_msgs/Float32MultiArray')

        -- Start the client application (c++ node)
        -- Do not forget to change paths. 
        -- General instalation space of gorobots: /home/user/workspace/gorobots
        -- Maybe "/home/user/workspace/gorobots/projects/db_alpha/walking_neural_control/sim/catkin_ws/src/walking_neural_control/bin/walking_neural_control"
        result=sim.launchExecutable('../workspace/gorobots/projects/db_alpha/walking_neural_control/sim/catkin_ws/src/walking_neural_controller/bin/walking_neural_controller', MotorTopicName.." "..simulationTimeTopicName.." "..terminateControllerName.." "..startSimulationName.." "..pauseSimulationName.." "..stopSimulationName.." "..enableSyncModeName.." "..triggerNextStepName.." "..simulationStepDoneName.." "..simulationStateName.." "..CPGOutputName.." "..jointPositionsName.." "..testParametersName.." "..jointTorquesName.." "..jointLPFtorquesName.." "..jointVelocityName.." "..controllerTypeName.." "..SimulationType,0)
        if (result==false) then
            sim.displayDialog('Error','External ROS-Node not found',sim.dlgstyle_ok,false,nil,{0.8,0,0,0,0,0},{0.5,0,0,1,1,1})
        end
    end


    simROS.setParamBool('use_sim_time', false)
    simROS.publish(terminateControllerPub,{data=false})
    sim.setBoolParameter(sim.boolparam_rosinterface_donotrunmainscript,false)

    -- Send the controller type
    simROS.publish(controllerTypePub,{data=PorT})
    -- Publish controller type
    --if (PorT==true) then
	--simROS.publish(controllerTypePub,{data=true})
    --else
	--simROS.publish(controllerTypePub,{data=false})
    --end

    hello = true
end

--[[
Actuation: This part will be executed in each simulation step
--]]
if (sim_call_type==sim.childscriptcall_actuation) then
    
    -- Publish
    simROS.publish(terminateControllerPub,{data=false})
    simROS.publish(controllerTypePub,{data=PorT})
    simROS.publish(clockPub,{clock=simGetSimulationTime()})
end

--[[
Sensing: This part will be executed in each simulation step
--]]
if (sim_call_type==sim.childscriptcall_sensing) then
    
    -- Calculate dx and dt to get the velocity, based on current, previous (motor) position AND current, previous time 
    local currentPos=simGetJointPosition(FT_motor0)
    local dx=getAngleMinusAlpha(currentPos,previousPos)
    local currentTime=simGetSimulationTime()
    local dt=currentTime-previousTime

    -- Calculate the velocity based on dx and dt
    --local vFT0=dx/dt
    vFT0=dx/dt

    -- Update previous time and previous position
    previousPos=currentPos
    previousTime=currentTime

    -- Calculate ANGULAR velocity and Position
    angularVelocity = math.abs(vFT0) -- rad / s
    
    if math.abs(angularVelocity) < 100 then
        LPFAngVel = LPFAngVel - ( 0.04 * (LPFAngVel - angularVelocity) );
    end
    smaAngVel = smaAN(LPFAngVel)


    jointTorque=math.abs(sim.getJointForce(TC_motor0)) -- N*m
    LPFjointTorque = LPFjointTorque - (0.04 * (LPFjointTorque - jointTorque));
    smaTor = smaTO(LPFjointTorque)

    -- Joint Torques
    -- LPFJointTorque <==> Low-Pass-Filtered torque. Formula: LPFSignal = LPFSignal - (LPFbeta * (LPFSignal - OriginalSignal))
    -- LPFSignal = 0, LPFbeta = 0.04    

    -- TC_motors
    jointTorqueTC0=simGetJointForce(TC_motor0) -- N*m
    jointTorqueTC1=simGetJointForce(TC_motor1) -- N*m
    jointTorqueTC2=simGetJointForce(TC_motor2) -- N*m
    jointTorqueTC3=simGetJointForce(TC_motor3) -- N*m
    jointTorqueTC4=simGetJointForce(TC_motor4) -- N*m
    jointTorqueTC5=simGetJointForce(TC_motor5) -- N*m
    LPFjointTorqueTC0 = LPFjointTorqueTC0 - (0.04 * (LPFjointTorqueTC0 - math.abs(jointTorqueTC0)));
    LPFjointTorqueTC1 = LPFjointTorqueTC1 - (0.04 * (LPFjointTorqueTC1 - math.abs(jointTorqueTC1)));
    LPFjointTorqueTC2 = LPFjointTorqueTC2 - (0.04 * (LPFjointTorqueTC2 - math.abs(jointTorqueTC2)));
    LPFjointTorqueTC3 = LPFjointTorqueTC3 - (0.04 * (LPFjointTorqueTC3 - math.abs(jointTorqueTC3)));
    LPFjointTorqueTC4 = LPFjointTorqueTC4 - (0.04 * (LPFjointTorqueTC4 - math.abs(jointTorqueTC4)));
    LPFjointTorqueTC5 = LPFjointTorqueTC5 - (0.04 * (LPFjointTorqueTC5 - math.abs(jointTorqueTC5)));
    smaTorTC0 = smaTO(LPFjointTorqueTC0)
    smaTorTC1 = smaTO(LPFjointTorqueTC1)
    smaTorTC2 = smaTO(LPFjointTorqueTC2)
    smaTorTC3 = smaTO(LPFjointTorqueTC3)
    smaTorTC4 = smaTO(LPFjointTorqueTC4)
    smaTorTC5 = smaTO(LPFjointTorqueTC5)
    
    -- CF_motors
    jointTorqueCF0=simGetJointForce(CF_motor0) -- N*m
    jointTorqueCF1=simGetJointForce(CF_motor1) -- N*m
    jointTorqueCF2=simGetJointForce(CF_motor2) -- N*m
    jointTorqueCF3=simGetJointForce(CF_motor3) -- N*m
    jointTorqueCF4=simGetJointForce(CF_motor4) -- N*m
    jointTorqueCF5=simGetJointForce(CF_motor5) -- N*m
    LPFjointTorqueCF0 = LPFjointTorqueCF0 - (0.04 * (LPFjointTorqueCF0 - math.abs(jointTorqueCF0)));
    LPFjointTorqueCF1 = LPFjointTorqueCF1 - (0.04 * (LPFjointTorqueCF1 - math.abs(jointTorqueCF1)));
    LPFjointTorqueCF2 = LPFjointTorqueCF2 - (0.04 * (LPFjointTorqueCF2 - math.abs(jointTorqueCF2)));
    LPFjointTorqueCF3 = LPFjointTorqueCF3 - (0.04 * (LPFjointTorqueCF3 - math.abs(jointTorqueCF3)));
    LPFjointTorqueCF4 = LPFjointTorqueCF4 - (0.04 * (LPFjointTorqueCF4 - math.abs(jointTorqueCF4)));
    LPFjointTorqueCF5 = LPFjointTorqueCF5 - (0.04 * (LPFjointTorqueCF5 - math.abs(jointTorqueCF5)));
    smaTorCF0 = smaTO(LPFjointTorqueCF0)
    smaTorCF1 = smaTO(LPFjointTorqueCF1)
    smaTorCF2 = smaTO(LPFjointTorqueCF2)
    smaTorCF3 = smaTO(LPFjointTorqueCF3)
    smaTorCF4 = smaTO(LPFjointTorqueCF4)
    smaTorCF5 = smaTO(LPFjointTorqueCF5)
    
    -- FT_motors
    jointTorqueFT0=simGetJointForce(FT_motor0) -- N*m
    jointTorqueFT1=simGetJointForce(FT_motor1) -- N*m
    jointTorqueFT2=simGetJointForce(FT_motor2) -- N*m
    jointTorqueFT3=simGetJointForce(FT_motor3) -- N*m
    jointTorqueFT4=simGetJointForce(FT_motor4) -- N*m
    jointTorqueFT5=simGetJointForce(FT_motor5) -- N*m
    LPFjointTorqueFT0 = LPFjointTorqueFT0 - (0.04 * (LPFjointTorqueFT0 - math.abs(jointTorqueFT0)));
    LPFjointTorqueFT1 = LPFjointTorqueFT1 - (0.04 * (LPFjointTorqueFT1 - math.abs(jointTorqueFT1)));
    LPFjointTorqueFT2 = LPFjointTorqueFT2 - (0.04 * (LPFjointTorqueFT2 - math.abs(jointTorqueFT2)));
    LPFjointTorqueFT3 = LPFjointTorqueFT3 - (0.04 * (LPFjointTorqueFT3 - math.abs(jointTorqueFT3)));
    LPFjointTorqueFT4 = LPFjointTorqueFT4 - (0.04 * (LPFjointTorqueFT4 - math.abs(jointTorqueFT4)));
    LPFjointTorqueFT5 = LPFjointTorqueFT5 - (0.04 * (LPFjointTorqueFT5 - math.abs(jointTorqueFT5)));
    smaTorFT0 = smaTO(LPFjointTorqueFT0)
    smaTorFT1 = smaTO(LPFjointTorqueFT1)
    smaTorFT2 = smaTO(LPFjointTorqueFT2)
    smaTorFT3 = smaTO(LPFjointTorqueFT3)
    smaTorFT4 = smaTO(LPFjointTorqueFT4)
    smaTorFT5 = smaTO(LPFjointTorqueFT5)


    linearVelocity, aVelocity=sim.getObjectVelocity(dummesvin) -- m/s

--    print('\n')
--    print('LPF: ')
--    print(roundToNthDecimal(LPFAngVel,5));
--    print('\n')

    testParameters[6]=smaAngVel
    testParameters[7]=math.sqrt((linearVelocity[1]*linearVelocity[1]) + (linearVelocity[2]*linearVelocity[2]))
    testParameters[8]=smaTorCF0
    testParameters[9]=smaTorFT0
    testParameters[10]=linearVelocity[1]
    testParameters[11]=linearVelocity[2]

    stepCounter = stepCounter + 1
    testParameters[12] = stepCounter

    -- Joint Positions

    -- TC_motors
    posTC0=simGetJointPosition(TC_motor0)
    posTC1=simGetJointPosition(TC_motor1)
    posTC2=simGetJointPosition(TC_motor2)
    posTC3=simGetJointPosition(TC_motor3)
    posTC4=simGetJointPosition(TC_motor4)
    posTC5=simGetJointPosition(TC_motor5)
    
    -- CF_motors
    posCF0=simGetJointPosition(CF_motor0)
    posCF1=simGetJointPosition(CF_motor1)
    posCF2=simGetJointPosition(CF_motor2)
    posCF3=simGetJointPosition(CF_motor3)
    posCF4=simGetJointPosition(CF_motor4)
    posCF5=simGetJointPosition(CF_motor5)
    
    -- FT_motors
    posFT0=simGetJointPosition(FT_motor0)
    posFT1=simGetJointPosition(FT_motor1)
    posFT2=simGetJointPosition(FT_motor2)
    posFT3=simGetJointPosition(FT_motor3)
    posFT4=simGetJointPosition(FT_motor4)
    posFT5=simGetJointPosition(FT_motor5)

    -- Joint Velocities

    -- TC_motors
    velTC0=simGetJointVelocity(TC_motor0)
    velTC1=simGetJointVelocity(TC_motor1)
    velTC2=simGetJointVelocity(TC_motor2)
    velTC3=simGetJointVelocity(TC_motor3)
    velTC4=simGetJointVelocity(TC_motor4)
    velTC5=simGetJointVelocity(TC_motor5)
    
    -- CF_motors
    velCF0=simGetJointVelocity(CF_motor0)
    velCF1=simGetJointVelocity(CF_motor1)
    velCF2=simGetJointVelocity(CF_motor2)
    velCF3=simGetJointVelocity(CF_motor3)
    velCF4=simGetJointVelocity(CF_motor4)
    velCF5=simGetJointVelocity(CF_motor5)

    -- FT_motors
    velFT0=simGetJointVelocity(CF_motor0)
    velFT1=simGetJointVelocity(FT_motor1)
    velFT2=simGetJointVelocity(FT_motor2)
    velFT3=simGetJointVelocity(FT_motor3)
    velFT4=simGetJointVelocity(FT_motor4)
    velFT5=simGetJointVelocity(FT_motor5)


    -- Data arrays
    position_array={posTC0,posTC1,posTC2,posTC3,posTC4,posTC5,posCF0,posCF1,posCF2,posCF3,posCF4,posCF5,posFT0,posFT1,posFT2,posFT3,posFT4,posFT5}
    torque_array={jointTorqueTC0,jointTorqueTC1,jointTorqueTC2,jointTorqueTC3,jointTorqueTC4,jointTorqueTC5,jointTorqueCF0,jointTorqueCF1,jointTorqueCF2,jointTorqueCF3,jointTorqueCF4,jointTorqueCF5,jointTorqueFT0,jointTorqueFT1,jointTorqueFT2,jointTorqueFT3,jointTorqueFT4,jointTorqueFT5}
    LPFtorque_array={smaTorTC0,smaTorTC1,smaTorTC2,smaTorTC3,smaTorTC4,smaTorTC5,smaTorCF0,smaTorCF1,smaTorCF2,smaTorCF3,smaTorCF4,smaTorCF5,smaTorFT0,smaTorFT1,smaTorFT2,smaTorFT3,smaTorFT4,smaTorFT5}    
    vel_array={velTC0,velTC1,velTC2,velTC3,velTC4,velTC5,velCF0,velCF1,velCF2,velCF3,velCF4,velCF5,velFT0,velFT1,velFT2,velFT3,velFT4,velFT5}

    -- Publish
    simROS.publish(jointPositionsPub,{data=position_array})
    simROS.publish(testParametersPub,{data=testParameters})
    simROS.publish(jointTorquePub,{data=torque_array})
    simROS.publish(jointLPFtorquePub,{data=LPFtorque_array})
    simROS.publish(jointVelocityPub,{data=vel_array})
end

--[[
Sensing: This part will be executed one time just before a simulation ends
--]]
if (sim_call_type==sim.childscriptcall_cleanup) then

    print('terminating')
    simROS.publish(terminateControllerPub,{data=true})
    sim.setBoolParameter(sim.boolparam_rosinterface_donotrunmainscript,true)

    -- Wait for the signal to arive at the nodes
    waitTimer=0
    while( waitTimer < 3000 ) do
        print('Waited '..waitTimer..' of 1000.')
        waitTimer = waitTimer+1
        simROS.publish(clockPub,{clock=simGetSimulationTime()+waitTimer})
        simROS.publish(terminateControllerPub,{data=true})
    end

    print('C++ Node stopped.')

    -- Close UI
    simUI.destroy(ui)

    -- Clode ROS related stuff
    if not pluginNotFound then
        -- Send termination signal to external ROS nodes
        simROS.publish(terminateControllerPub,{data=true})

        -- Terminate remaining local notes
        simROS.shutdownSubscriber(CPGOutputSub)
        simROS.shutdownSubscriber(MotorSub)
        simROS.shutdownPublisher(clockPub)
        simROS.shutdownPublisher(jointPositionsPub)
        simROS.shutdownPublisher(testParametersPub)
        simROS.shutdownPublisher(terminateControllerPub)
    end

    print('Lua child script stopped.')
end

